<?php include("includes/header.php"); ?>
<body>
<?php include("includes/navigation.php"); ?>
<!-- page wapper-->
<div class="columns-container">
    <div class="container" id="columns">
        <!-- breadcrumb -->
        <div class="breadcrumb clearfix">
            <a class="home" href="#" title="Return to Home">Home</a>
            <span class="navigation-pipe">&nbsp;</span>
            <span class="navigation_page">Payment result</span>
        </div>
        <!-- ./breadcrumb -->
        <!-- page heading-->
        <h2 class="page-heading no-line">
            <span class="page-heading-title2">Payment Result</span>
        </h2>
        <!-- ../page heading-->
        <div class="page-content page-order">
            <div class="order-fail-content">
                <div class="box-border">
                    <h3>Transaction Details</h3>
                    <p>Sorry that your transaction is failed. Please return to checkout page to try again. Thank you.</p>
                    <div class="cart_navigation">
                        <a class="prev-btn" href="checkout.php">Return to checkout</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- ./page wapper-->
<?php include("includes/footer.php"); ?>
<?php include("includes/scripts.php"); ?>
</body>
</html>