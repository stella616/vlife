<?php include("includes/header.php"); ?>
<link rel="stylesheet" type="text/css" href="assets/lib/jquery.bxslider/jquery.bxslider.css"/>
<body>
<?php include("includes/navigation.php"); ?>

<div class="columns-container">
    <div class="container" id="columns">
        <!-- breadcrumb -->
        <div class="breadcrumb clearfix">
            <a class="home" href="#" title="Return to Home">Home</a>
            <span class="navigation-pipe">&nbsp;</span>
            <span class="navigation_page">Search</span>
        </div>
        <!-- ./breadcrumb -->
        <!-- row -->
        <div class="row">
            <!-- Center colunm-->
            <div class="center_column col-xs-12 col-sm-12" id="center_column">
                <!-- view-product-list-->
                <h2 class="page-heading">
                    <span class="page-heading-title2">"search"</span>
                </h2>
                <span>123 items found.</span>
                <div id="view-product-list" class="view-product-list">
                    <div class="display-product-option">
                        <div class="show-product-item">
                            <select name="">
                                <option value="">Show 18</option>
                                <option value="">Show 20</option>
                                <option value="">Show 50</option>
                                <option value="">Show 100</option>
                            </select>
                        </div>
                        <div class="sort-product">
                            <select>
                                <option value="">Product Name</option>
                                <option value="">Price</option>
                            </select>
                            <div class="sort-product-icon">
                                <i class="fa fa-sort-alpha-asc"></i>
                            </div>
                        </div>
                        <ul>
                            <li class="view-as-grid selected">
                                <span>grid</span>
                            </li>
                            <li class="view-as-list">
                                <span>list</span>
                            </li>
                        </ul>
                    </div>
                    <!-- PRODUCT LIST -->
                    <ul class="row product-list grid">
                        <li class="col-sx-6 col-sm-4 col-md-3">
                            <div class="product-container">
                                <div class="left-block">
                                    <a href="detail.php">
                                        <div class="discounted-percentage product-list-discount-percentage">-15%</div>
                                        <img class="img-responsive" alt="product" src="assets/data/p40.jpg"/>
                                    </a>
                                    <div class="quick-view">
                                        <a title="Add to my wishlist" class="heart" href="#"></a>
                                        <a title="Add to compare" class="compare" href="#"></a>
                                        <a title="Quick view" class="search" href="#"></a>
                                    </div>
                                    <div class="add-to-cart">
                                        <a title="Add to Cart" href="#add">Add to Cart</a>
                                    </div>
                                </div>
                                <div class="right-block">
                                    <h5 class="product-name"><a href="detail.php">Maecenas consequat mauris</a></h5>
                                    <div class="content_price">
                                        <p class="price product-price">Retail price</p>
                                        <p>$138,95</p>
                                    </div>
                                    <div class="vcoin-wrapper vcoin-discounted-wrapper">
                                        <span class="product-coin discounted-vcoin">84.15</span>
                                        <span class="product-coin original-vcoin">99</span>
                                    </div>
                                    <div class="info-orther">
                                        <p>Item Code: #453217907</p>
                                        <p class="availability">Availability: <span>In stock</span></p>
                                        <div class="product-desc">
                                            Vestibulum eu odio. Suspendisse potenti. Morbi mollis tellus ac sapien.
                                            Praesent egestas tristique nibh. Nullam dictum felis eu pede mollis pretium.
                                            Fusce egestas elit eget lorem. In auctor lobortis lacus. Suspendisse
                                            faucibus, nunc et pellentesque egestas, lacus ante convallis tellus, vitae
                                            iaculis lacus elit id tortor.
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </li>
                        <li class="col-sx-6 col-sm-4 col-md-3">
                            <div class="product-container">
                                <div class="left-block">
                                    <a href="detail.php">
                                        <img class="img-responsive" alt="product" src="assets/data/p35.jpg"/>
                                    </a>
                                    <div class="quick-view">
                                        <a title="Add to my wishlist" class="heart" href="#"></a>
                                        <a title="Add to compare" class="compare" href="#"></a>
                                        <a title="Quick view" class="search" href="#"></a>
                                    </div>
                                    <div class="add-to-cart">
                                        <a title="Add to Cart" href="#add">Add to Cart</a>
                                    </div>
                                </div>
                                <div class="right-block">
                                    <h5 class="product-name"><a href="detail.php">Maecenas consequat mauris</a></h5>
                                    <div class="content_price">
                                        <p class="price product-price">Retail price</p>
                                        <p>$138,95</p>
                                    </div>
                                    <div class="vcoin-wrapper">
                                        <span class="product-coin original-vcoin">99</span>
                                    </div>
                                    <div class="info-orther">
                                        <p>Item Code: #453217907</p>
                                        <p class="availability">Availability: <span>In stock</span></p>
                                        <div class="product-desc">
                                            Vestibulum eu odio. Suspendisse potenti. Morbi mollis tellus ac sapien.
                                            Praesent egestas tristique nibh. Nullam dictum felis eu pede mollis pretium.
                                            Fusce egestas elit eget lorem. In auctor lobortis lacus. Suspendisse
                                            faucibus, nunc et pellentesque egestas, lacus ante convallis tellus, vitae
                                            iaculis lacus elit id tortor.
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </li>
                        <li class="col-sx-6 col-sm-4 col-md-3">
                            <div class="product-container">
                                <div class="left-block">
                                    <a href="detail.php">
                                        <div class="discounted-percentage product-list-discount-percentage">-15%</div>
                                        <img class="img-responsive" alt="product" src="assets/data/p41.jpg"/>
                                    </a>
                                    <div class="quick-view">
                                        <a title="Add to my wishlist" class="heart" href="#"></a>
                                        <a title="Add to compare" class="compare" href="#"></a>
                                        <a title="Quick view" class="search" href="#"></a>
                                    </div>
                                    <div class="add-to-cart">
                                        <a title="Add to Cart" href="#add">Add to Cart</a>
                                    </div>
                                </div>
                                <div class="right-block">
                                    <h5 class="product-name"><a href="detail.php">Maecenas consequat mauris</a></h5>
                                    <div class="content_price">
                                        <p class="price product-price">Retail price</p>
                                        <p>$138,95</p>
                                    </div>
                                    <div class="vcoin-wrapper vcoin-discounted-wrapper">
                                        <span class="product-coin discounted-vcoin">84.15</span>
                                        <span class="product-coin original-vcoin">99</span>
                                    </div>
                                    <div class="info-orther">
                                        <p>Item Code: #453217907</p>
                                        <p class="availability">Availability: <span>In stock</span></p>
                                        <div class="product-desc">
                                            Vestibulum eu odio. Suspendisse potenti. Morbi mollis tellus ac sapien.
                                            Praesent egestas tristique nibh. Nullam dictum felis eu pede mollis pretium.
                                            Fusce egestas elit eget lorem. In auctor lobortis lacus. Suspendisse
                                            faucibus, nunc et pellentesque egestas, lacus ante convallis tellus, vitae
                                            iaculis lacus elit id tortor.
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </li>
                        <li class="col-sx-6 col-sm-4 col-md-3">
                            <div class="product-container">
                                <div class="left-block">
                                    <a href="detail.php">
                                        <img class="img-responsive" alt="product" src="assets/data/p37.jpg"/>
                                    </a>
                                    <div class="quick-view">
                                        <a title="Add to my wishlist" class="heart" href="#"></a>
                                        <a title="Add to compare" class="compare" href="#"></a>
                                        <a title="Quick view" class="search" href="#"></a>
                                    </div>
                                    <div class="add-to-cart">
                                        <a title="Add to Cart" href="#add">Add to Cart</a>
                                    </div>
                                </div>
                                <div class="right-block">
                                    <h5 class="product-name"><a href="detail.php">Maecenas consequat mauris</a></h5>
                                    <div class="content_price">
                                        <p class="price product-price">Retail price</p>
                                        <p>$138,95</p>
                                    </div>
                                    <div class="vcoin-wrapper">
                                        <span class="product-coin original-vcoin">99</span>
                                    </div>
                                    <div class="info-orther">
                                        <p>Item Code: #453217907</p>
                                        <p class="availability">Availability: <span>In stock</span></p>
                                        <div class="product-desc">
                                            Vestibulum eu odio. Suspendisse potenti. Morbi mollis tellus ac sapien.
                                            Praesent egestas tristique nibh. Nullam dictum felis eu pede mollis pretium.
                                            Fusce egestas elit eget lorem. In auctor lobortis lacus. Suspendisse
                                            faucibus, nunc et pellentesque egestas, lacus ante convallis tellus, vitae
                                            iaculis lacus elit id tortor.
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </li>
                        <li class="col-sx-6 col-sm-4 col-md-3">
                            <div class="product-container">
                                <div class="left-block">
                                    <a href="detail.php">
                                        <div class="discounted-percentage product-list-discount-percentage">-15%</div>
                                        <img class="img-responsive" alt="product" src="assets/data/p38.jpg"/>
                                    </a>
                                    <div class="quick-view">
                                        <a title="Add to my wishlist" class="heart" href="#"></a>
                                        <a title="Add to compare" class="compare" href="#"></a>
                                        <a title="Quick view" class="search" href="#"></a>
                                    </div>
                                    <div class="add-to-cart">
                                        <a title="Add to Cart" href="#add">Add to Cart</a>
                                    </div>
                                </div>
                                <div class="right-block">
                                    <h5 class="product-name"><a href="detail.php">Maecenas consequat mauris</a></h5>
                                    <div class="content_price">
                                        <p class="price product-price">Retail price</p>
                                        <p>$138,95</p>
                                    </div>
                                    <div class="vcoin-wrapper vcoin-discounted-wrapper">
                                        <span class="product-coin discounted-vcoin">84.15</span>
                                        <span class="product-coin original-vcoin">99</span>
                                    </div>
                                    <div class="info-orther">
                                        <p>Item Code: #453217907</p>
                                        <p class="availability">Availability: <span>In stock</span></p>
                                        <div class="product-desc">
                                            Vestibulum eu odio. Suspendisse potenti. Morbi mollis tellus ac sapien.
                                            Praesent egestas tristique nibh. Nullam dictum felis eu pede mollis pretium.
                                            Fusce egestas elit eget lorem. In auctor lobortis lacus. Suspendisse
                                            faucibus, nunc et pellentesque egestas, lacus ante convallis tellus, vitae
                                            iaculis lacus elit id tortor.
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </li>
                        <li class="col-sx-6 col-sm-4 col-md-3">
                            <div class="product-container">
                                <div class="left-block">
                                    <a href="detail.php">
                                        <img class="img-responsive" alt="product" src="assets/data/p39.jpg"/>
                                    </a>
                                    <div class="quick-view">
                                        <a title="Add to my wishlist" class="heart" href="#"></a>
                                        <a title="Add to compare" class="compare" href="#"></a>
                                        <a title="Quick view" class="search" href="#"></a>
                                    </div>
                                    <div class="add-to-cart">
                                        <a title="Add to Cart" href="#add">Add to Cart</a>
                                    </div>
                                </div>
                                <div class="right-block">
                                    <h5 class="product-name"><a href="detail.php">Maecenas consequat mauris</a></h5>
                                    <div class="content_price">
                                        <p class="price product-price">Retail price</p>
                                        <p>$138,95</p>
                                    </div>
                                    <div class="vcoin-wrapper">
                                        <span class="product-coin original-vcoin">99</span>
                                    </div>
                                    <div class="info-orther">
                                        <p>Item Code: #453217907</p>
                                        <p class="availability">Availability: <span>In stock</span></p>
                                        <div class="product-desc">
                                            Vestibulum eu odio. Suspendisse potenti. Morbi mollis tellus ac sapien.
                                            Praesent egestas tristique nibh. Nullam dictum felis eu pede mollis pretium.
                                            Fusce egestas elit eget lorem. In auctor lobortis lacus. Suspendisse
                                            faucibus, nunc et pellentesque egestas, lacus ante convallis tellus, vitae
                                            iaculis lacus elit id tortor.
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </li>
                        <li class="col-sx-6 col-sm-4 col-md-3">
                            <div class="product-container">
                                <div class="left-block">
                                    <a href="detail.php">
                                        <div class="discounted-percentage product-list-discount-percentage">-15%</div>
                                        <img class="img-responsive" alt="product"
                                             src="assets/data/product-300x366.jpg"/>
                                    </a>
                                    <div class="quick-view">
                                        <a title="Add to my wishlist" class="heart" href="#"></a>
                                        <a title="Add to compare" class="compare" href="#"></a>
                                        <a title="Quick view" class="search" href="#"></a>
                                    </div>
                                    <div class="add-to-cart">
                                        <a title="Add to Cart" href="#add">Add to Cart</a>
                                    </div>
                                </div>
                                <div class="right-block">
                                    <h5 class="product-name"><a href="detail.php">Maecenas consequat mauris</a></h5>
                                    <div class="content_price">
                                        <p class="price product-price">Retail price</p>
                                        <p>$138,95</p>
                                    </div>
                                    <div class="vcoin-wrapper vcoin-discounted-wrapper">
                                        <span class="product-coin discounted-vcoin">84.15</span>
                                        <span class="product-coin original-vcoin">99</span>
                                    </div>
                                    <div class="info-orther">
                                        <p>Item Code: #453217907</p>
                                        <p class="availability">Availability: <span>In stock</span></p>
                                        <div class="product-desc">
                                            Vestibulum eu odio. Suspendisse potenti. Morbi mollis tellus ac sapien.
                                            Praesent egestas tristique nibh. Nullam dictum felis eu pede mollis pretium.
                                            Fusce egestas elit eget lorem. In auctor lobortis lacus. Suspendisse
                                            faucibus, nunc et pellentesque egestas, lacus ante convallis tellus, vitae
                                            iaculis lacus elit id tortor.
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </li>
                        <li class="col-sx-6 col-sm-4 col-md-3">
                            <div class="product-container">
                                <div class="left-block">
                                    <a href="detail.php">
                                        <img class="img-responsive" alt="product" src="assets/data/p36.jpg"/>
                                    </a>
                                    <div class="quick-view">
                                        <a title="Add to my wishlist" class="heart" href="#"></a>
                                        <a title="Add to compare" class="compare" href="#"></a>
                                        <a title="Quick view" class="search" href="#"></a>
                                    </div>
                                    <div class="add-to-cart">
                                        <a title="Add to Cart" href="#add">Add to Cart</a>
                                    </div>
                                </div>
                                <div class="right-block">
                                    <h5 class="product-name"><a href="detail.php">Maecenas consequat mauris</a></h5>
                                    <div class="content_price">
                                        <p class="price product-price">Retail price</p>
                                        <p>$138,95</p>
                                    </div>
                                    <div class="vcoin-wrapper">
                                        <span class="product-coin original-vcoin">99</span>
                                    </div>
                                    <div class="info-orther">
                                        <p>Item Code: #453217907</p>
                                        <p class="availability">Availability: <span>In stock</span></p>
                                        <div class="product-desc">
                                            Vestibulum eu odio. Suspendisse potenti. Morbi mollis tellus ac sapien.
                                            Praesent egestas tristique nibh. Nullam dictum felis eu pede mollis pretium.
                                            Fusce egestas elit eget lorem. In auctor lobortis lacus. Suspendisse
                                            faucibus, nunc et pellentesque egestas, lacus ante convallis tellus, vitae
                                            iaculis lacus elit id tortor.
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </li>
                        <li class="col-sx-6 col-sm-4 col-md-3">
                            <div class="product-container">
                                <div class="left-block">
                                    <a href="detail.php">
                                        <div class="discounted-percentage product-list-discount-percentage">-15%</div>
                                        <img class="img-responsive" alt="product" src="assets/data/p42.jpg"/>
                                    </a>
                                    <div class="quick-view">
                                        <a title="Add to my wishlist" class="heart" href="#"></a>
                                        <a title="Add to compare" class="compare" href="#"></a>
                                        <a title="Quick view" class="search" href="#"></a>
                                    </div>
                                    <div class="add-to-cart">
                                        <a title="Add to Cart" href="#add">Add to Cart</a>
                                    </div>
                                </div>
                                <div class="right-block">
                                    <h5 class="product-name"><a href="detail.php">Maecenas consequat mauris</a></h5>
                                    <div class="content_price">
                                        <p class="price product-price">Retail price</p>
                                        <p>$138,95</p>
                                    </div>
                                    <div class="vcoin-wrapper vcoin-discounted-wrapper">
                                        <span class="product-coin discounted-vcoin">84.15</span>
                                        <span class="product-coin original-vcoin">99</span>
                                    </div>
                                    <div class="info-orther">
                                        <p>Item Code: #453217907</p>
                                        <p class="availability">Availability: <span>In stock</span></p>
                                        <div class="product-desc">
                                            Vestibulum eu odio. Suspendisse potenti. Morbi mollis tellus ac sapien.
                                            Praesent egestas tristique nibh. Nullam dictum felis eu pede mollis pretium.
                                            Fusce egestas elit eget lorem. In auctor lobortis lacus. Suspendisse
                                            faucibus, nunc et pellentesque egestas, lacus ante convallis tellus, vitae
                                            iaculis lacus elit id tortor.
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </li>
                        <li class="col-sx-6 col-sm-4 col-md-3">
                            <div class="product-container">
                                <div class="left-block">
                                    <a href="detail.php">
                                        <img class="img-responsive" alt="product" src="assets/data/p34.jpg"/>
                                    </a>
                                    <div class="quick-view">
                                        <a title="Add to my wishlist" class="heart" href="#"></a>
                                        <a title="Add to compare" class="compare" href="#"></a>
                                        <a title="Quick view" class="search" href="#"></a>
                                    </div>
                                    <div class="add-to-cart">
                                        <a title="Add to Cart" href="#add">Add to Cart</a>
                                    </div>
                                </div>
                                <div class="right-block">
                                    <h5 class="product-name"><a href="detail.php">Maecenas consequat mauris</a></h5>
                                    <div class="content_price">
                                        <p class="price product-price">Retail price</p>
                                        <p>$138,95</p>
                                    </div>
                                    <div class="vcoin-wrapper">
                                        <span class="product-coin original-vcoin">99</span>
                                    </div>
                                    <div class="info-orther">
                                        <p>Item Code: #453217907</p>
                                        <p class="availability">Availability: <span>In stock</span></p>
                                        <div class="product-desc">
                                            Vestibulum eu odio. Suspendisse potenti. Morbi mollis tellus ac sapien.
                                            Praesent egestas tristique nibh. Nullam dictum felis eu pede mollis pretium.
                                            Fusce egestas elit eget lorem. In auctor lobortis lacus. Suspendisse
                                            faucibus, nunc et pellentesque egestas, lacus ante convallis tellus, vitae
                                            iaculis lacus elit id tortor.
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </li>
                        <li class="col-sx-6 col-sm-4 col-md-3">
                            <div class="product-container">
                                <div class="left-block">
                                    <a href="detail.php">
                                        <div class="discounted-percentage product-list-discount-percentage">-15%</div>
                                        <img class="img-responsive" alt="product" src="assets/data/p89.jpg"/>
                                    </a>
                                    <div class="quick-view">
                                        <a title="Add to my wishlist" class="heart" href="#"></a>
                                        <a title="Add to compare" class="compare" href="#"></a>
                                        <a title="Quick view" class="search" href="#"></a>
                                    </div>
                                    <div class="add-to-cart">
                                        <a title="Add to Cart" href="#add">Add to Cart</a>
                                    </div>
                                </div>
                                <div class="right-block">
                                    <h5 class="product-name"><a href="detail.php">Maecenas consequat mauris</a></h5>
                                    <div class="content_price">
                                        <p class="price product-price">Retail price</p>
                                        <p>$138,95</p>
                                    </div>
                                    <div class="vcoin-wrapper vcoin-discounted-wrapper">
                                        <span class="product-coin discounted-vcoin">84.15</span>
                                        <span class="product-coin original-vcoin">99</span>
                                    </div>
                                    <div class="info-orther">
                                        <p>Item Code: #453217907</p>
                                        <p class="availability">Availability: <span>In stock</span></p>
                                        <div class="product-desc">
                                            Vestibulum eu odio. Suspendisse potenti. Morbi mollis tellus ac sapien.
                                            Praesent egestas tristique nibh. Nullam dictum felis eu pede mollis pretium.
                                            Fusce egestas elit eget lorem. In auctor lobortis lacus. Suspendisse
                                            faucibus, nunc et pellentesque egestas, lacus ante convallis tellus, vitae
                                            iaculis lacus elit id tortor.
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </li>
                        <li class="col-sx-6 col-sm-4 col-md-3">
                            <div class="product-container">
                                <div class="left-block">
                                    <a href="detail.php">
                                        <img class="img-responsive" alt="product" src="assets/data/p90.jpg"/>
                                    </a>
                                    <div class="quick-view">
                                        <a title="Add to my wishlist" class="heart" href="#"></a>
                                        <a title="Add to compare" class="compare" href="#"></a>
                                        <a title="Quick view" class="search" href="#"></a>
                                    </div>
                                    <div class="add-to-cart">
                                        <a title="Add to Cart" href="#add">Add to Cart</a>
                                    </div>
                                </div>
                                <div class="right-block">
                                    <h5 class="product-name"><a href="detail.php">Maecenas consequat mauris</a></h5>
                                    <div class="content_price">
                                        <p class="price product-price">Retail price</p>
                                        <p>$138,95</p>
                                    </div>
                                    <div class="vcoin-wrapper">
                                        <span class="product-coin original-vcoin">99</span>
                                    </div>
                                    <div class="info-orther">
                                        <p>Item Code: #453217907</p>
                                        <p class="availability">Availability: <span>In stock</span></p>
                                        <div class="product-desc">
                                            Vestibulum eu odio. Suspendisse potenti. Morbi mollis tellus ac sapien.
                                            Praesent egestas tristique nibh. Nullam dictum felis eu pede mollis pretium.
                                            Fusce egestas elit eget lorem. In auctor lobortis lacus. Suspendisse
                                            faucibus, nunc et pellentesque egestas, lacus ante convallis tellus, vitae
                                            iaculis lacus elit id tortor.
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </li>
                        <li class="col-sx-6 col-sm-4 col-md-3">
                            <div class="product-container">
                                <div class="left-block">
                                    <a href="detail.php">
                                        <div class="discounted-percentage product-list-discount-percentage">-15%</div>
                                        <img class="img-responsive" alt="product" src="assets/data/p91.jpg"/>
                                    </a>
                                    <div class="quick-view">
                                        <a title="Add to my wishlist" class="heart" href="#"></a>
                                        <a title="Add to compare" class="compare" href="#"></a>
                                        <a title="Quick view" class="search" href="#"></a>
                                    </div>
                                    <div class="add-to-cart">
                                        <a title="Add to Cart" href="#add">Add to Cart</a>
                                    </div>
                                </div>
                                <div class="right-block">
                                    <h5 class="product-name"><a href="detail.php">Maecenas consequat mauris</a></h5>
                                    <div class="content_price">
                                        <p class="price product-price">Retail price</p>
                                        <p>$138,95</p>
                                    </div>
                                    <div class="vcoin-wrapper vcoin-discounted-wrapper">
                                        <span class="product-coin discounted-vcoin">84.15</span>
                                        <span class="product-coin original-vcoin">99</span>
                                    </div>
                                    <div class="info-orther">
                                        <p>Item Code: #453217907</p>
                                        <p class="availability">Availability: <span>In stock</span></p>
                                        <div class="product-desc">
                                            Vestibulum eu odio. Suspendisse potenti. Morbi mollis tellus ac sapien.
                                            Praesent egestas tristique nibh. Nullam dictum felis eu pede mollis pretium.
                                            Fusce egestas elit eget lorem. In auctor lobortis lacus. Suspendisse
                                            faucibus, nunc et pellentesque egestas, lacus ante convallis tellus, vitae
                                            iaculis lacus elit id tortor.
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </li>
                        <li class="col-sx-6 col-sm-4 col-md-3">
                            <div class="product-container">
                                <div class="left-block">
                                    <a href="detail.php">
                                        <img class="img-responsive" alt="product" src="assets/data/p92.jpg"/>
                                    </a>
                                    <div class="quick-view">
                                        <a title="Add to my wishlist" class="heart" href="#"></a>
                                        <a title="Add to compare" class="compare" href="#"></a>
                                        <a title="Quick view" class="search" href="#"></a>
                                    </div>
                                    <div class="add-to-cart">
                                        <a title="Add to Cart" href="#add">Add to Cart</a>
                                    </div>
                                </div>
                                <div class="right-block">
                                    <h5 class="product-name"><a href="detail.php">Maecenas consequat mauris</a></h5>
                                    <div class="content_price">
                                        <p class="price product-price">Retail price</p>
                                        <p>$138,95</p>
                                    </div>
                                    <div class="vcoin-wrapper">
                                        <span class="product-coin original-vcoin">99</span>
                                    </div>
                                    <div class="info-orther">
                                        <p>Item Code: #453217907</p>
                                        <p class="availability">Availability: <span>In stock</span></p>
                                        <div class="product-desc">
                                            Vestibulum eu odio. Suspendisse potenti. Morbi mollis tellus ac sapien.
                                            Praesent egestas tristique nibh. Nullam dictum felis eu pede mollis pretium.
                                            Fusce egestas elit eget lorem. In auctor lobortis lacus. Suspendisse
                                            faucibus, nunc et pellentesque egestas, lacus ante convallis tellus, vitae
                                            iaculis lacus elit id tortor.
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </li>
                        <li class="col-sx-6 col-sm-4 col-md-3">
                            <div class="product-container">
                                <div class="left-block">
                                    <a href="detail.php">
                                        <div class="discounted-percentage product-list-discount-percentage">-15%</div>
                                        <img class="img-responsive" alt="product" src="assets/data/p93.jpg"/>
                                    </a>
                                    <div class="quick-view">
                                        <a title="Add to my wishlist" class="heart" href="#"></a>
                                        <a title="Add to compare" class="compare" href="#"></a>
                                        <a title="Quick view" class="search" href="#"></a>
                                    </div>
                                    <div class="add-to-cart">
                                        <a title="Add to Cart" href="#add">Add to Cart</a>
                                    </div>
                                </div>
                                <div class="right-block">
                                    <h5 class="product-name"><a href="detail.php">Maecenas consequat mauris</a></h5>
                                    <div class="content_price">
                                        <p class="price product-price">Retail price</p>
                                        <p>$138,95</p>
                                    </div>
                                    <div class="vcoin-wrapper vcoin-discounted-wrapper">
                                        <span class="product-coin discounted-vcoin">84.15</span>
                                        <span class="product-coin original-vcoin">99</span>
                                    </div>
                                    <div class="info-orther">
                                        <p>Item Code: #453217907</p>
                                        <p class="availability">Availability: <span>In stock</span></p>
                                        <div class="product-desc">
                                            Vestibulum eu odio. Suspendisse potenti. Morbi mollis tellus ac sapien.
                                            Praesent egestas tristique nibh. Nullam dictum felis eu pede mollis pretium.
                                            Fusce egestas elit eget lorem. In auctor lobortis lacus. Suspendisse
                                            faucibus, nunc et pellentesque egestas, lacus ante convallis tellus, vitae
                                            iaculis lacus elit id tortor.
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </li>
                        <li class="col-sx-6 col-sm-4 col-md-3">
                            <div class="product-container">
                                <div class="left-block">
                                    <a href="detail.php">
                                        <img class="img-responsive" alt="product" src="assets/data/p94.jpg"/>
                                    </a>
                                    <div class="quick-view">
                                        <a title="Add to my wishlist" class="heart" href="#"></a>
                                        <a title="Add to compare" class="compare" href="#"></a>
                                        <a title="Quick view" class="search" href="#"></a>
                                    </div>
                                    <div class="add-to-cart">
                                        <a title="Add to Cart" href="#add">Add to Cart</a>
                                    </div>
                                </div>
                                <div class="right-block">
                                    <h5 class="product-name"><a href="detail.php">Maecenas consequat mauris</a></h5>
                                    <div class="content_price">
                                        <p class="price product-price">Retail price</p>
                                        <p>$138,95</p>
                                    </div>
                                    <div class="vcoin-wrapper">
                                        <span class="product-coin original-vcoin">99</span>
                                    </div>
                                    <div class="info-orther">
                                        <p>Item Code: #453217907</p>
                                        <p class="availability">Availability: <span>In stock</span></p>
                                        <div class="product-desc">
                                            Vestibulum eu odio. Suspendisse potenti. Morbi mollis tellus ac sapien.
                                            Praesent egestas tristique nibh. Nullam dictum felis eu pede mollis pretium.
                                            Fusce egestas elit eget lorem. In auctor lobortis lacus. Suspendisse
                                            faucibus, nunc et pellentesque egestas, lacus ante convallis tellus, vitae
                                            iaculis lacus elit id tortor.
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </li>
                    </ul>
                    <!-- ./PRODUCT LIST -->
                </div>
                <!-- ./view-product-list-->
                <div class="sortPagiBar">
                    <div class="bottom-pagination">
                        <nav>
                            <ul class="pagination">
                                <li class="active"><a href="#">1</a></li>
                                <li><a href="#">2</a></li>
                                <li><a href="#">3</a></li>
                                <li><a href="#">4</a></li>
                                <li><a href="#">5</a></li>
                                <li>
                                    <a href="#" aria-label="Next">
                                        <span aria-hidden="true">Next &raquo;</span>
                                    </a>
                                </li>
                            </ul>
                        </nav>
                    </div>
                </div>
            </div>
            <!-- ./ Center colunm -->
        </div>
        <!-- ./row-->
    </div>
</div>
<?php include("includes/footer.php"); ?>
<?php include("includes/scripts.php"); ?>
</body>
</html>