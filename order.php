<?php include("includes/header.php"); ?>
<body>
<?php include("includes/navigation.php"); ?>
<!-- page wapper-->
<div class="columns-container">
    <div class="container" id="columns">
        <!-- breadcrumb -->
        <div class="breadcrumb clearfix">
            <a class="home" href="#" title="Return to Home">Home</a>
            <span class="navigation-pipe">&nbsp;</span>
            <span class="navigation_page">Your shopping cart</span>
        </div>
        <!-- ./breadcrumb -->
        <!-- page heading-->
        <h2 class="page-heading no-line">
            <span class="page-heading-title2">Shopping Cart Summary</span>
        </h2>
        <!-- ../page heading-->
        <div class="page-content page-order">
            <ul class="step">
                <li class="current-step"><span>01. Summary</span></li>
                <li><span>02. Sign in</span></li>
                <li><span>03. Address</span></li>
                <li><span>04. Shipping</span></li>
                <li><span>05. Payment</span></li>
            </ul>
            <div class="heading-counter warning">Your shopping cart contains:
                <span>1 Product</span>
            </div>
            <div class="order-detail-content">
                <div class="table-responsive">
                    <table class="table table-bordered cart_summary">
                        <thead>
                        <tr>
                            <th class="action"><i class="fa fa-trash-o"></i></th>
                            <th class="cart_product text-center">Product</th>
                            <th class="cart_description">Product Name</th>
                            <th class="text-center">Total VCoin</th>
                            <th class="text-center">Quantity</th>
                            <th class="text-center">Subtotal</th>
                        </tr>
                        </thead>
                        <tbody>
                        <tr>
                            <td class="action">
                                <a href="#"><i class="fa fa-close"></i></a>
                            </td>
                            <td class="cart_product">
                                <a href="#">
                                    <div class="discounted-percentage product-list-discount-percentage">-15%</div>
                                    <img src="assets/data/product-100x122.jpg" alt="Product">
                                </a>
                            </td>
                            <td class="cart_description">
                                <p class="product-name"><a href="#">Frederique Constant </a></p>
                                <small class="cart_ref">SKU : #123654999</small>
                                <br>
                                <small><a href="#">Color : Beige</a></small>
                                <br>
                                <small><a href="#">Size : S</a></small>
                            </td>
                            <td class="price">
                                <div class="vcoin-wrapper vcoin-discounted-wrapper">
                                    <span class="product-coin discounted-vcoin">84.15</span>
                                    <span class="product-coin original-vcoin">99</span>
                                </div>
                            </td>
                            <td class="qty">
                                <input class="form-control input-sm" type="text" value="1">
                                <a href="#"><i class="fa fa-caret-up"></i></a>
                                <a href="#"><i class="fa fa-caret-down"></i></a>
                            </td>
                            <td class="price">
                                <div class="vcoin-wrapper vcoin-discounted-wrapper">
                                    <span class="product-coin discounted-vcoin">84.15</span>
                                    <span class="product-coin original-vcoin">99</span>
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <td class="action">
                                <a href="#"><i class="fa fa-close"></i></a>
                            </td>
                            <td class="cart_product">
                                <a href="#"><img src="assets/data/product-100x122.jpg" alt="Product"></a>
                            </td>
                            <td class="cart_description">
                                <p class="product-name"><a href="#">Frederique Constant </a></p>
                                <small class="cart_ref">SKU : #123654999</small>
                                <br>
                                <small><a href="#">Color : Beige</a></small>
                                <br>
                                <small><a href="#">Size : S</a></small>
                            </td>
                            <td class="price">
                                <div class="vcoin-wrapper">
                                    <span class="product-coin original-vcoin">99</span>
                                </div>
                            </td>
                            <td class="qty">
                                <input class="form-control input-sm" type="text" value="1">
                                <a href="#"><i class="fa fa-caret-up"></i></a>
                                <a href="#"><i class="fa fa-caret-down"></i></a>
                            </td>
                            <td class="price">
                                <div class="vcoin-wrapper">
                                    <span class="product-coin original-vcoin">99</span>
                                </div>
                            </td>
                        </tr>
                        </tbody>
                        <tfoot>
                        <tr>
                            <td colspan="2" rowspan="2"></td>
                            <td colspan="3">Total products (tax incl.)</td>
                            <td colspan="2"><span class="product-coin original-vcoin">183.15</span></td>
                        </tr>
                        <tr>
                            <td colspan="3"><strong>Total</strong></td>
                            <td colspan="2"><strong><span class="product-coin original-vcoin">183.15</span></strong></td>
                        </tr>
                        </tfoot>
                    </table>
                </div>
                <div class="cart_navigation">
                    <a class="prev-btn" href="category.php">Continue shopping</a>
                    <a class="next-btn" href="checkout.php">Proceed to checkout</a>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- ./page wapper-->
<?php include("includes/footer.php"); ?>
<?php include("includes/scripts.php"); ?>
</body>
</html>