<?php include("includes/header.php"); ?>
<body>
<?php include("includes/navigation.php"); ?>
<!-- page wapper-->
<div class="columns-container">
    <div class="container" id="columns">
        <!-- breadcrumb -->
        <div class="breadcrumb clearfix">
            <a class="home" href="#" title="Return to Home">Home</a>
            <span class="navigation-pipe">&nbsp;</span>
            <span class="navigation_page">Forgot password</span>
        </div>
        <!-- ./breadcrumb -->
        <!-- page heading-->
        <h2 class="page-heading">
            <span class="page-heading-title2">Forgot Password</span>
        </h2>
        <!-- ../page heading-->
        <div class="page-content text-center">
            <div class="row">
                <div class="col-sm-12">
                    <div class="box-forgot-password box-authentication">
                        <h3>Forgot your password?</h3>
                        <div class="form-group">
                            <label for="email_register">Email address</label>
                            <input id="email_register" type="text" class="form-control">
                        </div>
                        <button class="button"><i class="fa fa-send"></i> Send reset email</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- ./page wapper-->
<?php include("includes/footer.php"); ?>
<?php include("includes/scripts.php"); ?>
</body>
</html>