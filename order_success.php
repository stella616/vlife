<?php include("includes/header.php"); ?>
<body>
<?php include("includes/navigation.php"); ?>
<!-- page wapper-->
<div class="columns-container">
    <div class="container" id="columns">
        <!-- breadcrumb -->
        <div class="breadcrumb clearfix">
            <a class="home" href="#" title="Return to Home">Home</a>
            <span class="navigation-pipe">&nbsp;</span>
            <span class="navigation_page">Payment result</span>
        </div>
        <!-- ./breadcrumb -->
        <!-- page heading-->
        <h2 class="page-heading no-line">
            <span class="page-heading-title2">Payment Result</span>
        </h2>
        <!-- ../page heading-->
        <div class="page-content page-order">
            <div class="order-success-content">
                <div class="box-border">
                    <h3>Transaction Details</h3>
                    <p>Thank you for shopping with Vlife. You shall receive a status update within 3 working days, if
                        not your order will be cancelled and vtoken will be refunded.</p>
                    <div class="table-responsive">
                        <table class="table table-bordered cart_summary">
                            <thead>
                            <tr>
                                <th>Payer Name</th>
                                <th>Transaction ID</th>
                                <th class="text-center">Order Status</th>
                            </tr>
                            </thead>
                            <tbody>
                            <tr>
                                <td>Kean Yeoh</td>
                                <td>X13vt64</td>
                                <td class="text-center">
                                    <span class="order-status order-status-received">Received</span>
                                </td>
                            </tr>
                            <tr>
                                <td>Kean Yeoh</td>
                                <td>X13vt64</td>
                                <td class="text-center">
                                    <span class="order-status order-status-processing">Processing</span>
                                </td>
                            </tr>
                            <tr>
                                <td>Kean Yeoh</td>
                                <td>X13vt64</td>
                                <td class="text-center">
                                    <span class="order-status order-status-delivered">Delivered</span>
                                </td>
                            </tr>
                            <tr>
                                <td>Kean Yeoh</td>
                                <td>X13vt64</td>
                                <td class="text-center">
                                    <span class="order-status order-status-shipped">Shipped</span>
                                </td>
                            </tr>
                            <tr>
                                <td>Kean Yeoh</td>
                                <td>X13vt64</td>
                                <td class="text-center">
                                    <span class="order-status order-status-completed">Completed</span>
                                </td>
                            </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
                <div class="box-border">
                    <h3>Product Details Current Transactions</h3>
                    <div class="table-responsive">
                        <table class="table table-bordered cart_summary">
                            <thead>
                            <tr>
                                <th>Product Name</th>
                                <th class="text-center">Quantity</th>
                                <th class="text-center">VCoin</th>
                            </tr>
                            </thead>
                            <tbody>
                            <tr>
                                <td>As Seen on TV Perfect Pancake Maker Pan</td>
                                <td class="text-center">2</td>
                                <td class="text-center">
                                    <div class="product-vcoin-text">24</div>
                                    <div class="product-vcoin"><img src="assets/data/option6/v_token_icon.png"/></div>
                                </td>
                            </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- ./page wapper-->
<?php include("includes/footer.php"); ?>
<?php include("includes/scripts.php"); ?>
</body>
</html>