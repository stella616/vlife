<?php include("includes/header.php"); ?>
<link rel="stylesheet" type="text/css" href="assets/lib/jquery.bxslider/jquery.bxslider.css"/>
<body>
<?php include("includes/navigation.php"); ?>
<div class="columns-container">
    <div class="container" id="columns">
        <!-- breadcrumb -->
        <div class="breadcrumb clearfix">
            <a class="home" href="#" title="Return to Home">Home</a>
            <span class="navigation-pipe">&nbsp;</span>
            <a href="#" title="Return to Home">Fashion</a>
            <span class="navigation-pipe">&nbsp;</span>
            <a href="#" title="Return to Home">Women</a>
            <span class="navigation-pipe">&nbsp;</span>
            <a href="#" title="Return to Home">Dresses</a>
            <span class="navigation-pipe">&nbsp;</span>
            <span class="navigation_page">Maecenas consequat mauris</span>
        </div>
        <!-- ./breadcrumb -->
        <!-- row -->
        <div class="row">

            <!-- Center colunm-->
            <div class="center_column col-xs-12 col-sm-12" id="center_column">
                <!-- Product -->
                <div id="product">
                    <div class="primary-box row">
                        <div class="pb-left-column col-xs-12 col-sm-5">
                            <!-- product-imge-->
                            <div class="product-image">
                                <div class="product-full">
                                    <img id="product-zoom" src='assets/data/product-s3-420x512.jpg'
                                         data-zoom-image="assets/data/product-s3-850x1036.jpg"/>
                                </div>
                                <div class="product-img-thumb" id="gallery_01">
                                    <ul class="owl-carousel" data-items="3" data-nav="true" data-dots="false"
                                        data-margin="21" data-loop="false">
                                        <li>
                                            <a href="#" data-image="assets/data/product-s3-420x512.jpg"
                                               data-zoom-image="assets/data/product-s3-850x1036.jpg">
                                                <img id="product-zoom" src="assets/data/product-s3-100x122.jpg"/>
                                            </a>
                                        </li>
                                        <li>
                                            <a href="#" data-image="assets/data/product-s2-420x512.jpg"
                                               data-zoom-image="assets/data/product-s2-850x1036.jpg">
                                                <img id="product-zoom" src="assets/data/product-s2-100x122.jpg"/>
                                            </a>
                                        </li>
                                        <li>
                                            <a href="#" data-image="assets/data/product-420x512.jpg"
                                               data-zoom-image="assets/data/product-850x1036.jpg">
                                                <img id="product-zoom" src="assets/data/product-100x122.jpg"/>
                                            </a>
                                        </li>
                                        <li>
                                            <a href="#" data-image="assets/data/product-s4-420x512.jpg"
                                               data-zoom-image="assets/data/product-s4-850x1036.jpg">
                                                <img id="product-zoom" src="assets/data/product-s4-100x122.jpg"/>
                                            </a>
                                        </li>
                                        <li>
                                            <a href="#" data-image="assets/data/product-s5-420x512.jpg"
                                               data-zoom-image="assets/data/product-s5-850x1036.jpg">
                                                <img id="product-zoom" src="assets/data/product-s5-100x122.jpg"/>
                                            </a>
                                        </li>
                                        <li>
                                            <a href="#" data-image="assets/data/product-s6-420x512.jpg"
                                               data-zoom-image="assets/data/product-s6-850x1036.jpg">
                                                <img id="product-zoom" src="assets/data/product-s6-100x122.jpg"/>
                                            </a>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                            <!-- product-imge-->
                        </div>
                        <div class="pb-right-column col-xs-12 col-sm-7">
                            <h1 class="product-name">Maecenas consequat mauris</h1>
                            <div class="content_price">
                                <p class="price product-price">Retail price</p>
                                <p>$138,95</p>
                            </div>
                            <div class="discounted-percentage">-15%</div>
                            <div class="vcoin-wrapper vcoin-discounted-wrapper">
                                <span class="product-coin discounted-vcoin">84.15</span>
                                <span class="product-coin original-vcoin">99</span>
                            </div>
                            <div class="info-orther clearfix">
                                <p>Item Code: #453217907</p>
                                <p>Availability: <span class="in-stock">In stock</span></p>
                                <p>Condition: New</p>
                                <div class="product-desc">
                                    Vestibulum eu odio. Suspendisse potenti. Morbi mollis tellus ac sapien. Praesent
                                    egestas tristique nibh. Nullam dictum felis eu pede mollis pretium.Fusce egestas
                                    elit eget lorem.
                                </div>
                                <div class="form-option">
                                    <p class="form-option-title">Available Options:</p>
                                    <div class="attributes">
                                        <div class="attribute-label">Color:</div>
                                        <div class="attribute-list">
                                            <ul class="list-color">
                                                <li><a href="#" style="background:#0c3b90;">red</a></li>
                                                <li class="active"><a href="#" style="background:#036c5d;">red</a></li>
                                                <li><a href="#" style="background:#5f2363;">red</a></li>
                                                <li><a href="#" style="background:#ffc000;">red</a></li>
                                                <li><a href="#" style="background:#36a93c;">red</a></li>
                                                <li><a href="#" style="background:#ff0000;">red</a></li>
                                            </ul>
                                        </div>
                                    </div>
                                    <div class="attributes">
                                        <div class="attribute-label">Qty:</div>
                                        <div class="attribute-list product-qty">
                                            <div class="qty">
                                                <input id="option-product-qty" type="text" value="1">
                                            </div>
                                            <div class="btn-plus">
                                                <a href="#" class="btn-plus-up">
                                                    <i class="fa fa-caret-up"></i>
                                                </a>
                                                <a href="#" class="btn-plus-down">
                                                    <i class="fa fa-caret-down"></i>
                                                </a>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="attributes">
                                        <div class="attribute-label">Size:</div>
                                        <div class="attribute-list">
                                            <select>
                                                <option value="1">X</option>
                                                <option value="2">XL</option>
                                                <option value="3">XXL</option>
                                            </select>
                                            <a id="size_chart" class="fancybox" href="assets/data/size-chart.jpg">Size
                                                Chart</a>
                                        </div>

                                    </div>
                                </div>
                                <div class="form-action">
                                    <div class="button-group">
                                        <a class="btn-add-cart" href="#">Add to cart</a>
                                    </div>
                                </div>
                                <div class="form-share">
                                    <div class="network-share">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- tab product -->
                    <div class="product-tab">
                        <ul class="nav-tab">
                            <li class="active">
                                <a aria-expanded="false" data-toggle="tab" href="#product-detail">Product Details</a>
                            </li>
                            <li>
                                <a aria-expanded="true" data-toggle="tab" href="#information">information</a>
                            </li>
                        </ul>
                        <div class="tab-container">
                            <div id="product-detail" class="tab-panel active">
                                <p>Morbi mollis tellus ac sapien. Nunc nec neque. Praesent nec nisl a purus blandit
                                    viverra. Nunc nec neque. Pellentesque auctor neque nec urna.</p>

                                <p>Curabitur suscipit suscipit tellus. Cras id dui. Nam ipsum risus, rutrum vitae,
                                    vestibulum eu, molestie vel, lacus. Class aptent taciti sociosqu ad litora torquent
                                    per conubia nostra, per inceptos hymenaeos. Maecenas vestibulum mollis diam.</p>

                                <p>Vestibulum facilisis, purus nec pulvinar iaculis, ligula mi congue nunc, vitae
                                    euismod ligula urna in dolor. Sed lectus. Phasellus leo dolor, tempus non, auctor
                                    et, hendrerit quis, nisi. Nam at tortor in tellus interdum sagittis. Pellentesque
                                    egestas, neque sit amet convallis pulvinar, justo nulla eleifend augue, ac auctor
                                    orci leo non est.</p>
                            </div>
                            <div id="information" class="tab-panel">
                                <table class="table table-bordered">
                                    <tr>
                                        <td width="200">Compositions</td>
                                        <td>Cotton</td>
                                    </tr>
                                    <tr>
                                        <td>Styles</td>
                                        <td>Girly</td>
                                    </tr>
                                    <tr>
                                        <td>Properties</td>
                                        <td>Colorful Dress</td>
                                    </tr>
                                </table>
                            </div>
                        </div>
                    </div>
                    <!-- ./tab product -->
                    <!-- box product -->
                    <div class="page-product-box">
                        <h3 class="heading">Related Products</h3>
                        <ul class="product-list owl-carousel" data-dots="false" data-loop="true" data-nav="true"
                            data-margin="30" data-autoplayTimeout="1000" data-autoplayHoverPause="true"
                            data-responsive='{"0":{"items":1},"600":{"items":3},"1000":{"items":4}}'>
                            <li>
                                <div class="product-container">
                                    <div class="left-block">
                                        <a href="#">
                                            <img class="img-responsive" alt="product" src="assets/data/p40.jpg"/>
                                        </a>
                                        <div class="quick-view">
                                            <a title="Add to my wishlist" class="heart" href="#"></a>
                                            <a title="Add to compare" class="compare" href="#"></a>
                                            <a title="Quick view" class="search" href="#"></a>
                                        </div>
                                        <div class="add-to-cart">
                                            <a title="Add to Cart" href="#add">Add to Cart</a>
                                        </div>
                                    </div>
                                    <div class="right-block">
                                        <h5 class="product-name"><a href="#">Maecenas consequat mauris</a></h5>
                                        <div class="content_price">
                                            <p class="price product-price">Retail price</p>
                                            <p>$138,95</p>
                                        </div>
                                        <div class="vcoin-wrapper">
                                            <span class="product-coin original-vcoin">99</span>
                                        </div>
                                    </div>
                                </div>
                            </li>
                            <li>
                                <div class="product-container">
                                    <div class="left-block">
                                        <a href="#">
                                            <div class="discounted-percentage product-list-discount-percentage">-15%</div>
                                            <img class="img-responsive" alt="product" src="assets/data/p35.jpg"/>
                                        </a>
                                        <div class="quick-view">
                                            <a title="Add to my wishlist" class="heart" href="#"></a>
                                            <a title="Add to compare" class="compare" href="#"></a>
                                            <a title="Quick view" class="search" href="#"></a>
                                        </div>
                                        <div class="add-to-cart">
                                            <a title="Add to Cart" href="#add">Add to Cart</a>
                                        </div>
                                    </div>
                                    <div class="right-block">
                                        <h5 class="product-name"><a href="#">Maecenas consequat mauris</a></h5>
                                        <div class="content_price">
                                            <p class="price product-price">Retail price</p>
                                            <p>$138,95</p>
                                        </div>
                                        <div class="vcoin-wrapper vcoin-discounted-wrapper">
                                            <span class="product-coin discounted-vcoin">84.15</span>
                                            <span class="product-coin original-vcoin">99</span>
                                        </div>
                                    </div>
                                </div>
                            </li>
                            <li>
                                <div class="product-container">
                                    <div class="left-block">
                                        <a href="#">
                                            <img class="img-responsive" alt="product" src="assets/data/p37.jpg"/>
                                        </a>
                                        <div class="quick-view">
                                            <a title="Add to my wishlist" class="heart" href="#"></a>
                                            <a title="Add to compare" class="compare" href="#"></a>
                                            <a title="Quick view" class="search" href="#"></a>
                                        </div>
                                        <div class="add-to-cart">
                                            <a title="Add to Cart" href="#add">Add to Cart</a>
                                        </div>
                                    </div>
                                    <div class="right-block">
                                        <h5 class="product-name"><a href="#">Maecenas consequat mauris</a></h5>
                                        <div class="content_price">
                                            <p class="price product-price">Retail price</p>
                                            <p>$138,95</p>
                                        </div>
                                        <div class="vcoin-wrapper">
                                            <span class="product-coin original-vcoin">99</span>
                                        </div>
                                    </div>
                                </div>
                            </li>
                            <li>
                                <div class="product-container">
                                    <div class="left-block">
                                        <a href="#">
                                            <div class="discounted-percentage product-list-discount-percentage">-15%</div>
                                            <img class="img-responsive" alt="product" src="assets/data/p39.jpg"/>
                                        </a>
                                        <div class="quick-view">
                                            <a title="Add to my wishlist" class="heart" href="#"></a>
                                            <a title="Add to compare" class="compare" href="#"></a>
                                            <a title="Quick view" class="search" href="#"></a>
                                        </div>
                                        <div class="add-to-cart">
                                            <a title="Add to Cart" href="#add">Add to Cart</a>
                                        </div>
                                    </div>
                                    <div class="right-block">
                                        <h5 class="product-name"><a href="#">Maecenas consequat mauris</a></h5>
                                        <div class="content_price">
                                            <p class="price product-price">Retail price</p>
                                            <p>$138,95</p>
                                        </div>
                                        <div class="vcoin-wrapper vcoin-discounted-wrapper">
                                            <span class="product-coin discounted-vcoin">84.15</span>
                                            <span class="product-coin original-vcoin">99</span>
                                        </div>
                                    </div>
                                </div>
                            </li>
                        </ul>
                    </div>
                    <!-- ./box product -->
                    <!-- box product -->
                    <div class="page-product-box">
                        <h3 class="heading">You might also like</h3>
                        <ul class="product-list owl-carousel" data-dots="false" data-loop="true" data-nav="true"
                            data-margin="30" data-autoplayTimeout="1000" data-autoplayHoverPause="true"
                            data-responsive='{"0":{"items":1},"600":{"items":3},"1000":{"items":4}}'>
                            <li>
                                <div class="product-container">
                                    <div class="left-block">
                                        <a href="#">
                                            <div class="discounted-percentage product-list-discount-percentage">-15%</div>
                                            <img class="img-responsive" alt="product" src="assets/data/p97.jpg"/>
                                        </a>
                                        <div class="quick-view">
                                            <a title="Add to my wishlist" class="heart" href="#"></a>
                                            <a title="Add to compare" class="compare" href="#"></a>
                                            <a title="Quick view" class="search" href="#"></a>
                                        </div>
                                        <div class="add-to-cart">
                                            <a title="Add to Cart" href="#add">Add to Cart</a>
                                        </div>
                                    </div>
                                    <div class="right-block">
                                        <h5 class="product-name"><a href="#">Maecenas consequat mauris</a></h5>
                                        <div class="content_price">
                                            <p class="price product-price">Retail price</p>
                                            <p>$138,95</p>
                                        </div>
                                        <div class="vcoin-wrapper vcoin-discounted-wrapper">
                                            <span class="product-coin discounted-vcoin">84.15</span>
                                            <span class="product-coin original-vcoin">99</span>
                                        </div>
                                    </div>
                                </div>
                            </li>
                            <li>
                                <div class="product-container">
                                    <div class="left-block">
                                        <a href="#">
                                            <img class="img-responsive" alt="product" src="assets/data/p34.jpg"/>
                                        </a>
                                        <div class="quick-view">
                                            <a title="Add to my wishlist" class="heart" href="#"></a>
                                            <a title="Add to compare" class="compare" href="#"></a>
                                            <a title="Quick view" class="search" href="#"></a>
                                        </div>
                                        <div class="add-to-cart">
                                            <a title="Add to Cart" href="#add">Add to Cart</a>
                                        </div>
                                    </div>
                                    <div class="right-block">
                                        <h5 class="product-name"><a href="#">Maecenas consequat mauris</a></h5>
                                        <div class="content_price">
                                            <p class="price product-price">Retail price</p>
                                            <p>$138,95</p>
                                        </div>
                                        <div class="vcoin-wrapper">
                                            <span class="product-coin original-vcoin">99</span>
                                        </div>
                                    </div>
                                </div>
                            </li>
                            <li>
                                <div class="product-container">
                                    <div class="left-block">
                                        <a href="#">
                                            <div class="discounted-percentage product-list-discount-percentage">-15%</div>
                                            <img class="img-responsive" alt="product" src="assets/data/p36.jpg"/>
                                        </a>
                                        <div class="quick-view">
                                            <a title="Add to my wishlist" class="heart" href="#"></a>
                                            <a title="Add to compare" class="compare" href="#"></a>
                                            <a title="Quick view" class="search" href="#"></a>
                                        </div>
                                        <div class="add-to-cart">
                                            <a title="Add to Cart" href="#add">Add to Cart</a>
                                        </div>
                                    </div>
                                    <div class="right-block">
                                        <h5 class="product-name"><a href="#">Maecenas consequat mauris</a></h5>
                                        <div class="content_price">
                                            <p class="price product-price">Retail price</p>
                                            <p>$138,95</p>
                                        </div>
                                        <div class="vcoin-wrapper vcoin-discounted-wrapper">
                                            <span class="product-coin discounted-vcoin">84.15</span>
                                            <span class="product-coin original-vcoin">99</span>
                                        </div>
                                    </div>
                                </div>
                            </li>
                            <li>
                                <div class="product-container">
                                    <div class="left-block">
                                        <a href="#">
                                            <img class="img-responsive" alt="product" src="assets/data/p36.jpg"/>
                                        </a>
                                        <div class="quick-view">
                                            <a title="Add to my wishlist" class="heart" href="#"></a>
                                            <a title="Add to compare" class="compare" href="#"></a>
                                            <a title="Quick view" class="search" href="#"></a>
                                        </div>
                                        <div class="add-to-cart">
                                            <a title="Add to Cart" href="#add">Add to Cart</a>
                                        </div>
                                    </div>
                                    <div class="right-block">
                                        <h5 class="product-name"><a href="#">Maecenas consequat mauris</a></h5>
                                        <div class="content_price">
                                            <p class="price product-price">Retail price</p>
                                            <p>$138,95</p>
                                        </div>
                                        <div class="vcoin-wrapper">
                                            <span class="product-coin original-vcoin">99</span>
                                        </div>
                                    </div>
                                </div>
                            </li>
                        </ul>
                    </div>
                    <!-- ./box product -->
                </div>
                <!-- Product -->
            </div>
            <!-- ./ Center colunm -->
        </div>
        <!-- ./row-->
    </div>
</div>
<!-- ./page wapper-->
<?php include("includes/footer.php"); ?>
<?php include("includes/scripts.php"); ?>
</body>
</html>