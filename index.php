<?php include("includes/header.php"); ?>
<body>
<?php include("includes/navigation.php"); ?>
<!-- Home slideder-->
<div id="home-slider">
    <div class="container">
        <div class="row">
            <div class="col-sm-8 homeslider-wrapper">
                <div class="homeslider">
                    <ul id="contenhomeslider-customPage">
                        <li><img alt="Funky roots" src="assets/data/option6/slide1.jpg" title="Funky roots"/></li>
                        <li><img alt="Funky roots" src="assets/data/option6/slide1.jpg" title="Funky roots"/></li>
                        <li><img alt="Funky roots" src="assets/data/option6/slide1.jpg" title="Funky roots"/></li>
                    </ul>
                    <div class="bx-control">
                        <div class="slide-prev">
                            <span id="bx-prev"></span>
                        </div>
                        <div id="bx-pager" class="slide-pager">
                            <a data-slide-index="0" href="">1</a>
                            <a data-slide-index="1" href="">2</a>
                            <a data-slide-index="2" href="">3</a>
                        </div>
                        <div class="slide-next">
                            <span id="bx-next"></span>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-sm-2 group-banner-slider group-banner-slider-left hide-mobile">
                <div class="item banner-opacity">
                    <a href="#"><img src="assets/data/option6/banner9.jpg" alt="Banner"></a>
                </div>
                <div class="item banner-opacity">
                    <a href="#"><img src="assets/data/option6/banner10.jpg" alt="Banner"></a>
                </div>
                <div class="banner-opacity">
                    <a href="#"><img src="assets/data/option6/banner11.jpg" alt="Banner"></a>
                </div>
            </div>
            <div class="col-sm-2 group-banner-slider group-banner-slider-right hide-mobile">
                <div class="item banner-opacity">
                    <a href="#"><img src="assets/data/option6/banner-top.jpg" alt="Banner"></a>
                </div>
                <div class="item banner-opacity">
                    <a href="#"><img src="assets/data/option6/banner12.jpg" alt="Banner"></a>
                </div>
                <div class="banner-opacity">
                    <a href="#"><img src="assets/data/option6/banner13.jpg" alt="Banner"></a>
                </div>
            </div>
            <div class="mobile-group-banner-slider hide-desktop">
                <div class="item banner-opacity">
                    <a href="#"><img src="assets/data/option6/banner9.jpg" alt="Banner"></a>
                </div>
                <div class="item banner-opacity">
                    <a href="#"><img src="assets/data/option6/banner10.jpg" alt="Banner"></a>
                </div>
                <div class="banner-opacity">
                    <a href="#"><img src="assets/data/option6/banner11.jpg" alt="Banner"></a>
                </div>
                <div class="item banner-opacity">
                    <a href="#"><img src="assets/data/option6/banner-top.jpg" alt="Banner"></a>
                </div>
                <div class="item banner-opacity">
                    <a href="#"><img src="assets/data/option6/banner12.jpg" alt="Banner"></a>
                </div>
                <div class="banner-opacity">
                    <a href="#"><img src="assets/data/option6/banner13.jpg" alt="Banner"></a>
                </div>
            </div>
            <div class="col-sm-12 header-top-right">
                <div class="group-banner-slide">
                    <div class="col-left">

                    </div>
                    <div class="col-right">


                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- END Home slideder-->
<div class="page-top">
    <div class="container">
        <!-- block-popular-cat -->
        <div class="popular-cat-wrapper">
            <div class="col-sm-4">
                <div class="block-popular-cat">
                    <div class="parent-categories">Health & Beauty</div>
                    <div class="block-popular-inner">
                        <div class="image banner-boder-zoom2">
                            <a href="#"><img src="assets/data/option6/popular1.jpg" alt="popular"></a>
                        </div>
                        <div class="sub-categories">
                            <ul class="hide-mobile">
                                <li><a href="#">Eye Shadow</a></li>
                                <li><a href="#">Eyes Care</a></li>
                                <li><a href="#">Lipstick</a></li>
                                <li><a href="#">Face Color</a></li>
                                <li><a href="#">Nails</a></li>
                            </ul>
                            <a href="#" class="more">More</a>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-sm-4">
                <div class="block-popular-cat">
                    <div class="parent-categories">Shoes & Accessories</div>
                    <div class="block-popular-inner">
                        <div class="image banner-boder-zoom2">
                            <a href="#"><img src="assets/data/option6/popular2.jpg" alt="popular"></a>
                        </div>
                        <div class="sub-categories">
                            <ul class="hide-mobile">
                                <li><a href="#">Shoes</a></li>
                                <li><a href="#">Leather Bags</a></li>
                                <li><a href="#">Tops</a></li>
                                <li><a href="#">Bottoms</a></li>
                                <li><a href="#">Glasses</a></li>
                            </ul>
                            <a href="#" class="more">More</a>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-sm-4">
                <div class="block-popular-cat">
                    <div class="parent-categories">Jewellry & Watches</div>
                    <div class="block-popular-inner">
                        <div class="image banner-boder-zoom2">
                            <a href="#"><img src="assets/data/option6/popular3.jpg" alt="popular"></a>
                        </div>
                        <div class="sub-categories">
                            <ul class="hide-mobile">
                                <li><a href="#">Golden Necklace</a></li>
                                <li><a href="#">Earrings</a></li>
                                <li><a href="#">Diamond Ring</a></li>
                                <li><a href="#">Rings</a></li>
                                <li><a href="#">Watches</a></li>
                            </ul>
                            <a href="#" class="more">More</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- ./block-popular-cat -->
        <!-- Baner bottom -->
        <div class="row banner-bottom">
            <div class="col-sm-6 item-left">
                <div class="banner-boder-zoom2">
                    <a href="#"><img alt="ads" class="img-responsive" src="assets/data/option6/banner1.jpg"/></a>
                </div>
            </div>
            <div class="col-sm-6 item-right">
                <div class="banner-boder-zoom">
                    <a href="#"><img alt="ads" class="img-responsive" src="assets/data/option6/banner2.jpg"/></a>
                </div>
            </div>
        </div>
        <!-- end banner bottom -->
    </div>
</div>
<!---->
<div class="content-page">
    <div class="container">
        <!-- featured category fashion -->
        <div class="category-featured fashion">
            <nav class="navbar nav-menu show-brand">
                <div class="container">
                    <!-- Brand and toggle get grouped for better mobile display -->
                    <div class="navbar-brand"><a href="#"><img alt="fashion" src="assets/data/icon-fashion.png"/>fashion</a>
                    </div>
                    <span class="toggle-menu"></span>
                    <!-- Collect the nav links, forms, and other content for toggling -->
                    <div class="collapse navbar-collapse">
                        <ul class="nav navbar-nav">
                            <li class="active"><a data-toggle="tab" href="#tab-4">Best salers</a></li>
                            <li><a data-toggle="tab" href="#tab-5">Specials</a></li>
                            <li><a data-toggle="tab" href="#tab-4">New Arrivals</a></li>
                            <li><a data-toggle="tab" href="#tab-5">Most Reviews</a></li>
                            <li><a data-toggle="tab" href="#tab-4">On Sales</a></li>
                            <li><a data-toggle="tab" href="#tab-5">Trending</a></li>
                        </ul>
                    </div><!-- /.navbar-collapse -->
                </div><!-- /.container-fluid -->
                <div id="elevator-1" class="floor-elevator">
                    <a href="#" class="btn-elevator up disabled fa fa-angle-up"></a>
                    <a href="#elevator-2" class="btn-elevator down fa fa-angle-down"></a>
                </div>
            </nav>
            <div class="product-featured clearfix">
                <div class="row">
                    <div class="col-sm-2 sub-category-wapper">
                        <ul class="sub-category-list">
                            <li><a href="#">Summer Dresses</a></li>
                            <li><a href="#">Casual Dresses</a></li>
                            <li><a href="#">Blouses</a></li>
                            <li><a href="#">Skirts</a></li>
                            <li><a href="#">Jumpsuits</a></li>
                            <li><a href="#">T-Shirts</a></li>
                            <li><a href="#">Jackets</a></li>
                            <li><a href="#">Bikinis</a></li>
                            <li><a href="#">Sunglasses</a></li>
                            <li><a href="#">Scarves</a></li>
                            <li><a href="#">Hair Accessories</a></li>

                        </ul>
                    </div>
                    <div class="col-sm-10 col-right-tab">
                        <div class="product-featured-tab-content">
                            <div class="tab-container">
                                <div class="tab-panel active" id="tab-4">
                                    <div class="box-left">
                                        <div class="banner-img">
                                            <a href="#"><img src="assets/data/option6/banner3.jpg" alt="Banner Product"></a>
                                        </div>
                                    </div>
                                    <div class="box-right">
                                        <ul class="product-list row">
                                            <li class="col-sm-4">
                                                <div class="right-block">
                                                    <h5 class="product-name"><a href="detail.html">Sexy Red Dress</a>
                                                    </h5>
                                                    <div class="content_price">
                                                        <p class="price product-price">Retail price</p>
                                                        <p>$138,95</p>
                                                    </div>
                                                    <div class="discounted-percentage">-15%</div>
                                                    <div class="vcoin-wrapper vcoin-discounted-wrapper">
                                                        <span class="product-coin discounted-vcoin">84.15</span>
                                                        <span class="product-coin original-vcoin">99</span>
                                                    </div>
                                                </div>
                                                <div class="left-block">
                                                    <a href="detail.html"><img class="img-responsive" alt="product"
                                                                               src="assets/data/option6/p48.jpg"/></a>
                                                    <div class="add-to-cart">
                                                        <a title="Add to Cart" href="#">Add to Cart</a>
                                                    </div>
                                                </div>
                                            </li>
                                            <li class="col-sm-4">
                                                <div class="right-block">
                                                    <h5 class="product-name"><a href="detail.html">Mesh Dress Taupe</a>
                                                    </h5>
                                                    <div class="content_price">
                                                        <p class="price product-price">Retail price</p>
                                                        <p>$138,95</p>
                                                    </div>
                                                    <div class="vcoin-wrapper">
                                                        <span class="product-coin original-vcoin">99</span>
                                                    </div>
                                                </div>
                                                <div class="left-block">
                                                    <a href="detail.html"><img class="img-responsive" alt="product"
                                                                               src="assets/data/option6/p49.jpg"/></a>
                                                    <div class="add-to-cart">
                                                        <a title="Add to Cart" href="#">Add to Cart</a>
                                                    </div>
                                                </div>
                                            </li>
                                            <li class="col-sm-4">
                                                <div class="right-block">
                                                    <h5 class="product-name"><a href="detail.html">Sexy Summer Dress</a>
                                                    </h5>
                                                    <div class="content_price">
                                                        <p class="price product-price">Retail price</p>
                                                        <p>$138,95</p>
                                                    </div>
                                                    <div class="discounted-percentage">-15%</div>
                                                    <div class="vcoin-wrapper vcoin-discounted-wrapper">
                                                        <span class="product-coin discounted-vcoin">84.15</span>
                                                        <span class="product-coin original-vcoin">99</span>
                                                    </div>
                                                </div>
                                                <div class="left-block">
                                                    <a href="detail.html"><img class="img-responsive" alt="product"
                                                                               src="assets/data/option6/p50.jpg"/></a>
                                                    <div class="add-to-cart">
                                                        <a title="Add to Cart" href="#">Add to Cart</a>
                                                    </div>
                                                </div>
                                            </li>
                                            <li class="col-sm-4">
                                                <div class="right-block">
                                                    <h5 class="product-name"><a href="detail.html">Comfor & Pretty
                                                            Dress</a></h5>
                                                    <div class="content_price">
                                                        <p class="price product-price">Retail price</p>
                                                        <p>$138,95</p>
                                                    </div>
                                                    <div class="vcoin-wrapper">
                                                        <span class="product-coin original-vcoin">99</span>
                                                    </div>
                                                </div>
                                                <div class="left-block">
                                                    <a href="detail.html"><img class="img-responsive" alt="product"
                                                                               src="assets/data/option6/p51.jpg"/></a>
                                                    <div class="add-to-cart">
                                                        <a title="Add to Cart" href="#">Add to Cart</a>
                                                    </div>
                                                </div>
                                            </li>
                                            <li class="col-sm-4">
                                                <div class="right-block">
                                                    <h5 class="product-name"><a href="detail.html">Sexy Perfect
                                                            Light</a></h5>
                                                    <div class="content_price">
                                                        <p class="price product-price">Retail price</p>
                                                        <p>$138,95</p>
                                                    </div>
                                                    <div class="discounted-percentage">-15%</div>
                                                    <div class="vcoin-wrapper vcoin-discounted-wrapper">
                                                        <span class="product-coin discounted-vcoin">84.15</span>
                                                        <span class="product-coin original-vcoin">99</span>
                                                    </div>
                                                </div>
                                                <div class="left-block">
                                                    <a href="detail.html"><img class="img-responsive" alt="product"
                                                                               src="assets/data/option6/p52.jpg"/></a>
                                                    <div class="add-to-cart">
                                                        <a title="Add to Cart" href="#">Add to Cart</a>
                                                    </div>
                                                </div>
                                            </li>
                                            <li class="col-sm-4">
                                                <div class="right-block">
                                                    <h5 class="product-name"><a href="detail.html">New Style & Fresh</a>
                                                    </h5>
                                                    <div class="content_price">
                                                        <p class="price product-price">Retail price</p>
                                                        <p>$138,95</p>
                                                    </div>
                                                    <div class="vcoin-wrapper">
                                                        <span class="product-coin original-vcoin">99</span>
                                                    </div>
                                                </div>
                                                <div class="left-block">
                                                    <a href="#"><img class="img-responsive" alt="product"
                                                                     src="assets/data/option6/p53.jpg"/></a>
                                                    <div class="add-to-cart">
                                                        <a title="Add to Cart" href="#">Add to Cart</a>
                                                    </div>
                                                </div>
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                                <div class="tab-panel" id="tab-5">
                                    <div class="box-left">
                                        <div class="banner-img">
                                            <a href="#"><img src="assets/data/option6/banner3.jpg" alt="Banner Product"></a>
                                        </div>
                                    </div>
                                    <div class="box-right">
                                        <ul class="product-list row">
                                            <li class="col-sm-4">
                                                <div class="right-block">
                                                    <h5 class="product-name"><a href="detail.html">Headphone &
                                                            earphone</a></h5>
                                                    <div class="content_price">
                                                        <p class="price product-price">Retail price</p>
                                                        <p>$138,95</p>
                                                    </div>
                                                    <div class="discounted-percentage">-15%</div>
                                                    <div class="vcoin-wrapper vcoin-discounted-wrapper">
                                                        <span class="product-coin discounted-vcoin">84.15</span>
                                                        <span class="product-coin original-vcoin">99</span>
                                                    </div>
                                                </div>
                                                <div class="left-block">
                                                    <a href="detail.html">
                                                        <img class="img-responsive" alt="product"
                                                             src="assets/data/option6/p53.jpg"/></a>
                                                    <div class="add-to-cart">
                                                        <a title="Add to Cart" href="#">Add to Cart</a>
                                                    </div>
                                                </div>
                                            </li>
                                            <li class="col-sm-4">
                                                <div class="right-block">
                                                    <h5 class="product-name"><a href="detail.html">Headphone &
                                                            earphone</a></h5>
                                                    <div class="content_price">
                                                        <p class="price product-price">Retail price</p>
                                                        <p>$138,95</p>
                                                    </div>
                                                    <div class="vcoin-wrapper">
                                                        <span class="product-coin original-vcoin">99</span>
                                                    </div>
                                                </div>
                                                <div class="left-block">
                                                    <a href="detail.html">
                                                        <img class="img-responsive" alt="product"
                                                             src="assets/data/option6/p52.jpg"/></a>
                                                    <div class="add-to-cart">
                                                        <a title="Add to Cart" href="#">Add to Cart</a>
                                                    </div>
                                                </div>
                                            </li>
                                            <li class="col-sm-4">
                                                <div class="right-block">
                                                    <h5 class="product-name"><a href="detail.html">Headphone &
                                                            earphone</a></h5>
                                                    <div class="content_price">
                                                        <p class="price product-price">Retail price</p>
                                                        <p>$138,95</p>
                                                    </div>
                                                    <div class="discounted-percentage">-15%</div>
                                                    <div class="vcoin-wrapper vcoin-discounted-wrapper">
                                                        <span class="product-coin discounted-vcoin">84.15</span>
                                                        <span class="product-coin original-vcoin">99</span>
                                                    </div>
                                                </div>
                                                <div class="left-block">
                                                    <a href="detail.html">
                                                        <img class="img-responsive" alt="product"
                                                             src="assets/data/option6/p51.jpg"/></a>
                                                    <div class="add-to-cart">
                                                        <a title="Add to Cart" href="#">Add to Cart</a>
                                                    </div>
                                                </div>
                                            </li>
                                            <li class="col-sm-4">
                                                <div class="right-block">
                                                    <h5 class="product-name"><a href="detail.html">Headphone &
                                                            earphone</a></h5>
                                                    <div class="content_price">
                                                        <p class="price product-price">Retail price</p>
                                                        <p>$138,95</p>
                                                    </div>
                                                    <div class="vcoin-wrapper">
                                                        <span class="product-coin original-vcoin">99</span>
                                                    </div>
                                                </div>
                                                <div class="left-block">
                                                    <a href="detail.html">
                                                        <img class="img-responsive" alt="product"
                                                             src="assets/data/option6/p50.jpg"/></a>
                                                    <div class="add-to-cart">
                                                        <a title="Add to Cart" href="#">Add to Cart</a>
                                                    </div>
                                                </div>
                                            </li>
                                            <li class="col-sm-4">
                                                <div class="right-block">
                                                    <h5 class="product-name"><a href="detail.html">Headphone &
                                                            earphone</a></h5>
                                                    <div class="content_price">
                                                        <p class="price product-price">Retail price</p>
                                                        <p>$138,95</p>
                                                    </div>
                                                    <div class="discounted-percentage">-15%</div>
                                                    <div class="vcoin-wrapper vcoin-discounted-wrapper">
                                                        <span class="product-coin discounted-vcoin">84.15</span>
                                                        <span class="product-coin original-vcoin">99</span>
                                                    </div>
                                                </div>
                                                <div class="left-block">
                                                    <a href="detail.html">
                                                        <img class="img-responsive" alt="product"
                                                             src="assets/data/option6/p49.jpg"/></a>
                                                    <div class="add-to-cart">
                                                        <a title="Add to Cart" href="#">Add to Cart</a>
                                                    </div>
                                                </div>
                                            </li>
                                            <li class="col-sm-4">
                                                <div class="right-block">
                                                    <h5 class="product-name"><a href="detail.html">Headphone &
                                                            earphone</a></h5>
                                                    <div class="content_price">
                                                        <p class="price product-price">Retail price</p>
                                                        <p>$138,95</p>
                                                    </div>
                                                    <div class="vcoin-wrapper">
                                                        <span class="product-coin original-vcoin">99</span>
                                                    </div>
                                                </div>
                                                <div class="left-block">
                                                    <a href="detail.html">
                                                        <img class="img-responsive" alt="product"
                                                             src="assets/data/option6/p48.jpg"/></a>
                                                    <div class="add-to-cart">
                                                        <a title="Add to Cart" href="#">Add to Cart</a>
                                                    </div>
                                                </div>
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- end featured category fashion -->
        <!-- featured category sports -->
        <div class="category-featured sports">
            <nav class="navbar nav-menu show-brand">
                <div class="container">
                    <!-- Brand and toggle get grouped for better mobile display -->
                    <div class="navbar-brand"><a href="#"><img alt="fashion"
                                                               src="assets/data/icon-sports.png"/>SPORTS</a></div>
                    <span class="toggle-menu"></span>
                    <!-- Collect the nav links, forms, and other content for toggling -->
                    <div class="collapse navbar-collapse">
                        <ul class="nav navbar-nav">
                            <li class="active"><a data-toggle="tab" href="#tab-6">Best salers</a></li>
                            <li><a data-toggle="tab" href="#tab-7">Specials</a></li>
                            <li><a data-toggle="tab" href="#tab-6">New Arrivals</a></li>
                            <li><a data-toggle="tab" href="#tab-7">Most Reviews</a></li>
                        </ul>
                    </div><!-- /.navbar-collapse -->
                </div><!-- /.container-fluid -->
                <div id="elevator-2" class="floor-elevator">
                    <a href="#elevator-1" class="btn-elevator up fa fa-angle-up"></a>
                    <a href="#elevator-3" class="btn-elevator down fa fa-angle-down"></a>
                </div>
            </nav>
            <div class="product-featured clearfix">
                <div class="row">
                    <div class="col-sm-2 sub-category-wapper">
                        <div class="owl-carousel-vertical" data-items="1" data-nav="true" data-dots="false"
                             data-loop="true">
                            <div class="item">
                                <ul class="sub-category-list">
                                    <li><a href="#">Tent</a></li>
                                    <li><a href="#">Hiking Shoes</a></li>
                                    <li><a href="#">Cycling Jerseys</a></li>
                                    <li><a href="#">Boxing</a></li>
                                    <li><a href="#">Fitness</a></li>
                                    <li><a href="#">Basketball Shoes</a></li>
                                    <li><a href="#">Carp Fishing</a></li>
                                    <li><a href="#">Bike Light</a></li>
                                    <li><a href="#">Sunglasses</a></li>
                                    <li><a href="#">Fishing Tackle Bags</a></li>
                                    <li><a href="#">Camping Stoves</a></li>
                                    <li><a href="#">Cycling Jerseys</a></li>
                                    <li><a href="#">Cycling Jerseys</a></li>
                                    <li><a href="#">Basketball Shoes</a></li>
                                </ul>
                            </div>
                            <div class="item">
                                <ul class="sub-category-list">
                                    <li><a href="#">Shoes</a></li>
                                    <li><a href="#">Casual Shoes</a></li>
                                    <li><a href="#">Sports Shoes</a></li>
                                    <li><a href="#">Adidas Shoes</a></li>
                                    <li><a href="#">Gas Shoes</a></li>
                                    <li><a href="#">Puma Shoes</a></li>
                                    <li><a href="#">Reebok Shoes</a></li>
                                    <li><a href="#">Woodland Shoes</a></li>
                                    <li><a href="#">Red tape Shoes</a></li>
                                    <li><a href="#">Nike Shoes</a></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-10 col-right-tab">
                        <div class="product-featured-tab-content">
                            <div class="tab-container">
                                <div class="tab-panel active" id="tab-6">
                                    <div class="box-left">
                                        <div class="banner-img">
                                            <a href="#"><img src="assets/data/option6/banner4.jpg" alt="Banner Product"></a>
                                        </div>
                                    </div>
                                    <div class="box-right">
                                        <ul class="product-list row">
                                            <li class="col-sm-4">
                                                <div class="right-block">
                                                    <h5 class="product-name"><a href="detail.html">Sexy Sport Shoes</a>
                                                    </h5>
                                                    <div class="content_price">
                                                        <p class="price product-price">Retail price</p>
                                                        <p>$138,95</p>
                                                    </div>
                                                    <div class="discounted-percentage">-15%</div>
                                                    <div class="vcoin-wrapper vcoin-discounted-wrapper">
                                                        <span class="product-coin discounted-vcoin">84.15</span>
                                                        <span class="product-coin original-vcoin">99</span>
                                                    </div>
                                                </div>
                                                <div class="left-block">
                                                    <a href="detail.html">
                                                        <img class="img-responsive" alt="product"
                                                             src="assets/data/option6/p55.jpg"/></a>
                                                    <div class="add-to-cart">
                                                        <a title="Add to Cart" href="#">Add to Cart</a>
                                                    </div>
                                                </div>
                                            </li>
                                            <li class="col-sm-4">
                                                <div class="right-block">
                                                    <h5 class="product-name"><a href="detail.html">Tennis Blue Hat</a>
                                                    </h5>
                                                    <div class="content_price">
                                                        <p class="price product-price">Retail price</p>
                                                        <p>$138,95</p>
                                                    </div>
                                                    <div class="vcoin-wrapper">
                                                        <span class="product-coin original-vcoin">99</span>
                                                    </div>
                                                </div>
                                                <div class="left-block">
                                                    <a href="detail.html">
                                                        <img class="img-responsive" alt="product"
                                                             src="assets/data/option6/p56.jpg"/></a>
                                                    <div class="add-to-cart">
                                                        <a title="Add to Cart" href="#">Add to Cart</a>
                                                    </div>
                                                </div>

                                            </li>
                                            <li class="col-sm-4">
                                                <div class="right-block">
                                                    <h5 class="product-name"><a href="detail.html">Blue T-Shirt</a></h5>
                                                    <div class="content_price">
                                                        <p class="price product-price">Retail price</p>
                                                        <p>$138,95</p>
                                                    </div>
                                                    <div class="discounted-percentage">-15%</div>
                                                    <div class="vcoin-wrapper vcoin-discounted-wrapper">
                                                        <span class="product-coin discounted-vcoin">84.15</span>
                                                        <span class="product-coin original-vcoin">99</span>
                                                    </div>
                                                </div>
                                                <div class="left-block">
                                                    <a href="detail.html">
                                                        <img class="img-responsive" alt="product"
                                                             src="assets/data/option6/p57.jpg"/></a>
                                                    <div class="add-to-cart">
                                                        <a title="Add to Cart" href="#">Add to Cart</a>
                                                    </div>
                                                </div>
                                            </li>
                                            <li class="col-sm-4">
                                                <div class="right-block">
                                                    <h5 class="product-name"><a href="detail.html">Tennis Racquet</a>
                                                    </h5>
                                                    <div class="content_price">
                                                        <p class="price product-price">Retail price</p>
                                                        <p>$138,95</p>
                                                    </div>
                                                    <div class="vcoin-wrapper">
                                                        <span class="product-coin original-vcoin">99</span>
                                                    </div>
                                                </div>
                                                <div class="left-block">
                                                    <a href="detail.html">
                                                        <img class="img-responsive" alt="product"
                                                             src="assets/data/option6/p58.jpg"/></a>
                                                    <div class="add-to-cart">
                                                        <a title="Add to Cart" href="#">Add to Cart</a>
                                                    </div>
                                                </div>
                                            </li>
                                            <li class="col-sm-4">
                                                <div class="right-block">
                                                    <h5 class="product-name"><a href="detail.html">Fashion & Sport</a>
                                                    </h5>
                                                    <div class="content_price">
                                                        <p class="price product-price">Retail price</p>
                                                        <p>$138,95</p>
                                                    </div>
                                                    <div class="discounted-percentage">-15%</div>
                                                    <div class="vcoin-wrapper vcoin-discounted-wrapper">
                                                        <span class="product-coin discounted-vcoin">84.15</span>
                                                        <span class="product-coin original-vcoin">99</span>
                                                    </div>
                                                </div>
                                                <div class="left-block">
                                                    <a href="detail.html">
                                                        <img class="img-responsive" alt="product"
                                                             src="assets/data/option6/p59.jpg"/></a>
                                                    <div class="add-to-cart">
                                                        <a title="Add to Cart" href="#">Add to Cart</a>
                                                    </div>
                                                </div>

                                            </li>
                                            <li class="col-sm-4">
                                                <div class="right-block">
                                                    <h5 class="product-name"><a href="detail.html">Versatile Package</a>
                                                    </h5>
                                                    <div class="content_price">
                                                        <p class="price product-price">Retail price</p>
                                                        <p>$138,95</p>
                                                    </div>
                                                    <div class="vcoin-wrapper">
                                                        <span class="product-coin original-vcoin">99</span>
                                                    </div>
                                                </div>
                                                <div class="left-block">
                                                    <a href="detail.html">
                                                        <img class="img-responsive" alt="product"
                                                             src="assets/data/option6/p60.jpg"/></a>
                                                    <div class="add-to-cart">
                                                        <a title="Add to Cart" href="#">Add to Cart</a>
                                                    </div>
                                                </div>
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                                <div class="tab-panel" id="tab-7">
                                    <div class="box-left">
                                        <div class="banner-img">
                                            <a href="#"><img src="assets/data/option6/banner4.jpg" alt="Banner Product"></a>
                                        </div>
                                    </div>
                                    <div class="box-right">
                                        <ul class="product-list row">
                                            <li class="col-sm-4">
                                                <div class="right-block">
                                                    <h5 class="product-name"><a href="detail.html">Headphone &
                                                            earphone</a></h5>
                                                    <div class="content_price">
                                                        <p class="price product-price">Retail price</p>
                                                        <p>$138,95</p>
                                                    </div>
                                                    <div class="discounted-percentage">-15%</div>
                                                    <div class="vcoin-wrapper vcoin-discounted-wrapper">
                                                        <span class="product-coin discounted-vcoin">84.15</span>
                                                        <span class="product-coin original-vcoin">99</span>
                                                    </div>
                                                </div>
                                                <div class="left-block">
                                                    <a href="detail.html">
                                                        <img class="img-responsive" alt="product"
                                                             src="assets/data/option6/p60.jpg"/></a>
                                                    <div class="add-to-cart">
                                                        <a title="Add to Cart" href="#">Add to Cart</a>
                                                    </div>
                                                </div>
                                            </li>
                                            <li class="col-sm-4">
                                                <div class="right-block">
                                                    <h5 class="product-name"><a href="detail.html">Headphone &
                                                            earphone</a></h5>
                                                    <div class="content_price">
                                                        <p class="price product-price">Retail price</p>
                                                        <p>$138,95</p>
                                                    </div>
                                                    <div class="vcoin-wrapper">
                                                        <span class="product-coin original-vcoin">99</span>
                                                    </div>
                                                </div>
                                                <div class="left-block">
                                                    <a href="detail.html">
                                                        <img class="img-responsive" alt="product"
                                                             src="assets/data/option6/p59.jpg"/></a>
                                                    <div class="add-to-cart">
                                                        <a title="Add to Cart" href="#">Add to Cart</a>
                                                    </div>
                                                </div>

                                            </li>
                                            <li class="col-sm-4">
                                                <div class="right-block">
                                                    <h5 class="product-name"><a href="detail.html">Headphone &
                                                            earphone</a></h5>
                                                    <div class="content_price">
                                                        <p class="price product-price">Retail price</p>
                                                        <p>$138,95</p>
                                                    </div>
                                                    <div class="discounted-percentage">-15%</div>
                                                    <div class="vcoin-wrapper vcoin-discounted-wrapper">
                                                        <span class="product-coin discounted-vcoin">84.15</span>
                                                        <span class="product-coin original-vcoin">99</span>
                                                    </div>
                                                </div>
                                                <div class="left-block">
                                                    <a href="#">
                                                        <img class="img-responsive" alt="product"
                                                             src="assets/data/option6/p58.jpg"/></a>
                                                    <div class="add-to-cart">
                                                        <a title="Add to Cart" href="#">Add to Cart</a>
                                                    </div>
                                                </div>
                                            </li>
                                            <li class="col-sm-4">
                                                <div class="right-block">
                                                    <h5 class="product-name"><a href="detail.html">Headphone &
                                                            earphone</a></h5>
                                                    <div class="content_price">
                                                        <p class="price product-price">Retail price</p>
                                                        <p>$138,95</p>
                                                    </div>
                                                    <div class="vcoin-wrapper">
                                                        <span class="product-coin original-vcoin">99</span>
                                                    </div>
                                                </div>
                                                <div class="left-block">
                                                    <a href="detail.html">
                                                        <img class="img-responsive" alt="product"
                                                             src="assets/data/option6/p57.jpg"/></a>
                                                    <div class="add-to-cart">
                                                        <a title="Add to Cart" href="#">Add to Cart</a>
                                                    </div>
                                                </div>
                                            </li>
                                            <li class="col-sm-4">
                                                <div class="right-block">
                                                    <h5 class="product-name"><a href="detail.html">Headphone &
                                                            earphone</a></h5>
                                                    <div class="content_price">
                                                        <p class="price product-price">Retail price</p>
                                                        <p>$138,95</p>
                                                    </div>
                                                    <div class="discounted-percentage">-15%</div>
                                                    <div class="vcoin-wrapper vcoin-discounted-wrapper">
                                                        <span class="product-coin discounted-vcoin">84.15</span>
                                                        <span class="product-coin original-vcoin">99</span>
                                                    </div>
                                                </div>
                                                <div class="left-block">
                                                    <a href="detail.html">
                                                        <img class="img-responsive" alt="product"
                                                             src="assets/data/option6/p56.jpg"/></a>
                                                    <div class="add-to-cart">
                                                        <a title="Add to Cart" href="#">Add to Cart</a>
                                                    </div>
                                                </div>

                                            </li>
                                            <li class="col-sm-4">
                                                <div class="right-block">
                                                    <h5 class="product-name"><a href="detail.html">Headphone &
                                                            earphone</a></h5>
                                                    <div class="content_price">
                                                        <p class="price product-price">Retail price</p>
                                                        <p>$138,95</p>
                                                    </div>
                                                    <div class="vcoin-wrapper">
                                                        <span class="product-coin original-vcoin">99</span>
                                                    </div>
                                                </div>
                                                <div class="left-block">
                                                    <a href="detail.html">
                                                        <img class="img-responsive" alt="product"
                                                             src="assets/data/option6/p55.jpg"/></a>
                                                    <div class="add-to-cart">
                                                        <a title="Add to Cart" href="#">Add to Cart</a>
                                                    </div>
                                                </div>
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- end featured category sports-->

        <!-- featured category electronic -->
        <div class="category-featured electronic">
            <nav class="navbar nav-menu show-brand">
                <div class="container">
                    <!-- Brand and toggle get grouped for better mobile display -->
                    <div class="navbar-brand"><a href="#"><img alt="fashion" src="assets/data/icon-electronic.png"/>ELECTRONICS</a>
                    </div>
                    <span class="toggle-menu"></span>
                    <!-- Collect the nav links, forms, and other content for toggling -->
                    <div class="collapse navbar-collapse">
                        <ul class="nav navbar-nav">
                            <li class="active"><a data-toggle="tab" href="#tab-8">Best salers</a></li>
                            <li><a data-toggle="tab" href="#tab-9">Specials</a></li>
                            <li><a data-toggle="tab" href="#tab-8">New Arrivals</a></li>
                            <li><a data-toggle="tab" href="#tab-9">Most Reviews</a></li>
                            <li><a data-toggle="tab" href="#tab-8">On Sales</a></li>
                            <li><a data-toggle="tab" href="#tab-9">Trending</a></li>
                        </ul>
                    </div><!-- /.navbar-collapse -->
                </div><!-- /.container-fluid -->
                <div id="elevator-3" class="floor-elevator">
                    <a href="#elevator-2" class="btn-elevator up fa fa-angle-up"></a>
                    <a href="#elevator-4" class="btn-elevator down fa fa-angle-down"></a>
                </div>
            </nav>
            <div class="product-featured clearfix">
                <div class="row">
                    <div class="col-sm-2 sub-category-wapper">
                        <ul class="sub-category-list">
                            <li><a href="#">SOOCOO S60</a></li>
                            <li><a href="#">Xiaomi Sports Camera</a></li>
                            <li><a href="#">Monopod</a></li>
                            <li><a href="#">MP3 Player</a></li>
                            <li><a href="#">Speaker</a></li>
                            <li><a href="#">Micro SD Card</a></li>
                            <li><a href="#">Smart Watches</a></li>
                            <li><a href="#">Smart Wristbands</a></li>
                            <li><a href="#">TV</a></li>
                            <li><a href="#">Projector</a></li>
                            <li><a href="#">Gaming</a></li>
                        </ul>
                    </div>
                    <div class="col-sm-10 col-right-tab">
                        <div class="product-featured-tab-content">
                            <div class="tab-container">
                                <div class="tab-panel active" id="tab-8">
                                    <div class="box-left">
                                        <div class="banner-img">
                                            <a href="#"><img src="assets/data/option6/banner5.jpg" alt="Banner Product"></a>
                                        </div>
                                    </div>
                                    <div class="box-right">
                                        <ul class="product-list row">
                                            <li class="col-sm-4">
                                                <div class="right-block">
                                                    <h5 class="product-name"><a href="detail.html">Cookoo Cooker</a>
                                                    </h5>
                                                    <div class="content_price">
                                                        <p class="price product-price">Retail price</p>
                                                        <p>$138,95</p>
                                                    </div>
                                                    <div class="discounted-percentage">-15%</div>
                                                    <div class="vcoin-wrapper vcoin-discounted-wrapper">
                                                        <span class="product-coin discounted-vcoin">84.15</span>
                                                        <span class="product-coin original-vcoin">99</span>
                                                    </div>
                                                </div>
                                                <div class="left-block">
                                                    <a href="detail.html">
                                                        <img class="img-responsive" alt="product"
                                                             src="assets/data/option6/p61.jpg"/></a>
                                                    <div class="add-to-cart">
                                                        <a title="Add to Cart" href="#">Add to Cart</a>
                                                    </div>
                                                </div>
                                            </li>
                                            <li class="col-sm-4">
                                                <div class="right-block">
                                                    <h5 class="product-name"><a href="detail.html">Green Cooker</a></h5>
                                                    <div class="content_price">
                                                        <p class="price product-price">Retail price</p>
                                                        <p>$138,95</p>
                                                    </div>
                                                    <div class="vcoin-wrapper">
                                                        <span class="product-coin original-vcoin">99</span>
                                                    </div>
                                                </div>
                                                <div class="left-block">
                                                    <a href="detail.html">
                                                        <img class="img-responsive" alt="product"
                                                             src="assets/data/option6/p62.jpg"/></a>
                                                    <div class="add-to-cart">
                                                        <a title="Add to Cart" href="#">Add to Cart</a>
                                                    </div>
                                                </div>
                                            </li>
                                            <li class="col-sm-4">
                                                <div class="right-block">
                                                    <h5 class="product-name"><a href="detail.html">Modern Liquidizer</a>
                                                    </h5>
                                                    <div class="content_price">
                                                        <p class="price product-price">Retail price</p>
                                                        <p>$138,95</p>
                                                    </div>
                                                    <div class="discounted-percentage">-15%</div>
                                                    <div class="vcoin-wrapper vcoin-discounted-wrapper">
                                                        <span class="product-coin discounted-vcoin">84.15</span>
                                                        <span class="product-coin original-vcoin">99</span>
                                                    </div>
                                                </div>
                                                <div class="left-block">
                                                    <a href="detail.html">
                                                        <img class="img-responsive" alt="product"
                                                             src="assets/data/option6/p63.jpg"/></a>
                                                    <div class="add-to-cart">
                                                        <a title="Add to Cart" href="#">Add to Cart</a>
                                                    </div>
                                                </div>
                                            </li>
                                            <li class="col-sm-4">
                                                <div class="right-block">
                                                    <h5 class="product-name"><a href="detail.html">Serial Nice
                                                            Cookers</a></h5>
                                                    <div class="content_price">
                                                        <p class="price product-price">Retail price</p>
                                                        <p>$138,95</p>
                                                    </div>
                                                    <div class="vcoin-wrapper">
                                                        <span class="product-coin original-vcoin">99</span>
                                                    </div>
                                                </div>
                                                <div class="left-block">
                                                    <a href="detail.html">
                                                        <img class="img-responsive" alt="product"
                                                             src="assets/data/option6/p64.jpg"/></a>
                                                    <div class="add-to-cart">
                                                        <a title="Add to Cart" href="#">Add to Cart</a>
                                                    </div>
                                                </div>
                                            </li>
                                            <li class="col-sm-4">
                                                <div class="right-block">
                                                    <h5 class="product-name"><a href="detail.html">Tilor Washing
                                                            Machine</a></h5>
                                                    <div class="content_price">
                                                        <p class="price product-price">Retail price</p>
                                                        <p>$138,95</p>
                                                    </div>
                                                    <div class="discounted-percentage">-15%</div>
                                                    <div class="vcoin-wrapper vcoin-discounted-wrapper">
                                                        <span class="product-coin discounted-vcoin">84.15</span>
                                                        <span class="product-coin original-vcoin">99</span>
                                                    </div>
                                                </div>
                                                <div class="left-block">
                                                    <a href="detail.html">
                                                        <img class="img-responsive" alt="product"
                                                             src="assets/data/option6/p65.jpg"/></a>
                                                    <div class="add-to-cart">
                                                        <a title="Add to Cart" href="#">Add to Cart</a>
                                                    </div>
                                                </div>
                                            </li>
                                            <li class="col-sm-4">
                                                <div class="right-block">
                                                    <h5 class="product-name"><a href="detail.html">Nano Washing
                                                            Machine</a></h5>
                                                    <div class="content_price">
                                                        <p class="price product-price">Retail price</p>
                                                        <p>$138,95</p>
                                                    </div>
                                                    <div class="vcoin-wrapper">
                                                        <span class="product-coin original-vcoin">99</span>
                                                    </div>
                                                </div>
                                                <div class="left-block">
                                                    <a href="detail.html">
                                                        <img class="img-responsive" alt="product"
                                                             src="assets/data/option6/p66.jpg"/></a>
                                                    <div class="add-to-cart">
                                                        <a title="Add to Cart" href="#">Add to Cart</a>
                                                    </div>
                                                </div>
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                                <div class="tab-panel" id="tab-9">
                                    <div class="box-left">
                                        <div class="banner-img">
                                            <a href="#"><img src="assets/data/option6/banner5.jpg" alt="Banner Product"></a>
                                        </div>
                                    </div>
                                    <div class="box-right">
                                        <ul class="product-list row">
                                            <li class="col-sm-4">
                                                <div class="right-block">
                                                    <h5 class="product-name"><a href="detail.html">Headphone &
                                                            earphone</a></h5>
                                                    <div class="content_price">
                                                        <p class="price product-price">Retail price</p>
                                                        <p>$138,95</p>
                                                    </div>
                                                    <div class="discounted-percentage">-15%</div>
                                                    <div class="vcoin-wrapper vcoin-discounted-wrapper">
                                                        <span class="product-coin discounted-vcoin">84.15</span>
                                                        <span class="product-coin original-vcoin">99</span>
                                                    </div>
                                                </div>
                                                <div class="left-block">
                                                    <a href="detail.html">
                                                        <img class="img-responsive" alt="product"
                                                             src="assets/data/option6/p66.jpg"/></a>
                                                    <div class="add-to-cart">
                                                        <a title="Add to Cart" href="#">Add to Cart</a>
                                                    </div>
                                                </div>
                                            </li>
                                            <li class="col-sm-4">
                                                <div class="right-block">
                                                    <h5 class="product-name"><a href="detail.html">Headphone &
                                                            earphone</a></h5>
                                                    <div class="content_price">
                                                        <p class="price product-price">Retail price</p>
                                                        <p>$138,95</p>
                                                    </div>
                                                    <div class="vcoin-wrapper">
                                                        <span class="product-coin original-vcoin">99</span>
                                                    </div>
                                                </div>
                                                <div class="left-block">
                                                    <a href="detail.html">
                                                        <img class="img-responsive" alt="product"
                                                             src="assets/data/option6/p65.jpg"/></a>
                                                    <div class="add-to-cart">
                                                        <a title="Add to Cart" href="#">Add to Cart</a>
                                                    </div>
                                                </div>
                                            </li>
                                            <li class="col-sm-4">
                                                <div class="right-block">
                                                    <h5 class="product-name"><a href="detail.html">Headphone &
                                                            earphone</a></h5>
                                                    <div class="content_price">
                                                        <p class="price product-price">Retail price</p>
                                                        <p>$138,95</p>
                                                    </div>
                                                    <div class="discounted-percentage">-15%</div>
                                                    <div class="vcoin-wrapper vcoin-discounted-wrapper">
                                                        <span class="product-coin discounted-vcoin">84.15</span>
                                                        <span class="product-coin original-vcoin">99</span>
                                                    </div>
                                                </div>
                                                <div class="left-block">
                                                    <a href="detail.html">
                                                        <img class="img-responsive" alt="product"
                                                             src="assets/data/option6/p64.jpg"/></a>
                                                    <div class="add-to-cart">
                                                        <a title="Add to Cart" href="#">Add to Cart</a>
                                                    </div>
                                                </div>
                                            </li>
                                            <li class="col-sm-4">
                                                <div class="right-block">
                                                    <h5 class="product-name"><a href="detail.html">Headphone &
                                                            earphone</a></h5>
                                                    <div class="content_price">
                                                        <p class="price product-price">Retail price</p>
                                                        <p>$138,95</p>
                                                    </div>
                                                    <div class="vcoin-wrapper">
                                                        <span class="product-coin original-vcoin">99</span>
                                                    </div>
                                                </div>
                                                <div class="left-block">
                                                    <a href="detail.html">
                                                        <img class="img-responsive" alt="product"
                                                             src="assets/data/option6/p63.jpg"/></a>
                                                    <div class="add-to-cart">
                                                        <a title="Add to Cart" href="#">Add to Cart</a>
                                                    </div>
                                                </div>
                                            </li>
                                            <li class="col-sm-4">
                                                <div class="right-block">
                                                    <h5 class="product-name"><a href="detail.html">Headphone &
                                                            earphone</a></h5>
                                                    <div class="content_price">
                                                        <p class="price product-price">Retail price</p>
                                                        <p>$138,95</p>
                                                    </div>
                                                    <div class="discounted-percentage">-15%</div>
                                                    <div class="vcoin-wrapper vcoin-discounted-wrapper">
                                                        <span class="product-coin discounted-vcoin">84.15</span>
                                                        <span class="product-coin original-vcoin">99</span>
                                                    </div>
                                                </div>
                                                <div class="left-block">
                                                    <a href="detail.html">
                                                        <img class="img-responsive" alt="product"
                                                             src="assets/data/option6/p62.jpg"/></a>
                                                    <div class="add-to-cart">
                                                        <a title="Add to Cart" href="#">Add to Cart</a>
                                                    </div>
                                                </div>
                                            </li>
                                            <li class="col-sm-4">
                                                <div class="right-block">
                                                    <h5 class="product-name"><a href="detail.html">Headphone &
                                                            earphone</a></h5>
                                                    <div class="content_price">
                                                        <p class="price product-price">Retail price</p>
                                                        <p>$138,95</p>
                                                    </div>
                                                    <div class="vcoin-wrapper">
                                                        <span class="product-coin original-vcoin">99</span>
                                                    </div>
                                                </div>
                                                <div class="left-block">
                                                    <a href="detail.html">
                                                        <img class="img-responsive" alt="product"
                                                             src="assets/data/option6/p61.jpg"/></a>
                                                    <div class="add-to-cart">
                                                        <a title="Add to Cart" href="#">Add to Cart</a>
                                                    </div>
                                                </div>
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- end featured category electronic-->
        <!-- featured category Digital -->
        <div class="category-featured digital">
            <nav class="navbar nav-menu show-brand">
                <div class="container">
                    <!-- Brand and toggle get grouped for better mobile display -->
                    <div class="navbar-brand"><a href="#"><img alt="fashion" src="assets/data/icon-digital.png"/>DIGITAL</a>
                    </div>
                    <span class="toggle-menu"></span>
                    <!-- Collect the nav links, forms, and other content for toggling -->
                    <div class="collapse navbar-collapse">
                        <ul class="nav navbar-nav">
                            <li class="active"><a data-toggle="tab" href="#tab-10">Best salers</a></li>
                            <li><a data-toggle="tab" href="#tab-11">Specials</a></li>
                            <li><a data-toggle="tab" href="#tab-10">New Arrivals</a></li>
                            <li><a data-toggle="tab" href="#tab-11">Most Reviews</a></li>
                        </ul>
                    </div><!-- /.navbar-collapse -->
                </div><!-- /.container-fluid -->
                <div id="elevator-4" class="floor-elevator">
                    <a href="#elevator-3" class="btn-elevator up fa fa-angle-up"></a>
                    <a href="#elevator-5" class="btn-elevator down fa fa-angle-down"></a>
                </div>
            </nav>
            <div class="product-featured clearfix">
                <div class="row">
                    <div class="col-sm-2 sub-category-wapper">
                        <ul class="sub-category-list">
                            <li><a href="#">Tablet selfie</a></li>
                            <li><a href="#">Laptop Batteries</a></li>
                            <li><a href="#">OTG flash drives</a></li>
                            <li><a href="#">Mouse</a></li>
                            <li><a href="#">Keyboard</a></li>
                            <li><a href="#">SSD</a></li>
                            <li><a href="#">Tenda router</a></li>
                            <li><a href="#">3D printer</a></li>
                            <li><a href="#">Laser Pens</a></li>
                            <li><a href="#">Printer Supplies</a></li>
                            <li><a href="#">Gaming</a></li>
                        </ul>
                    </div>
                    <div class="col-sm-10 col-right-tab">
                        <div class="product-featured-tab-content">
                            <div class="tab-container">
                                <div class="tab-panel active" id="tab-10">
                                    <div class="box-left">
                                        <div class="banner-img">
                                            <a href="#"><img src="assets/data/option6/banner6.jpg" alt="Banner Product"></a>
                                        </div>
                                    </div>
                                    <div class="box-right">
                                        <ul class="product-list row">
                                            <li class="col-sm-4">
                                                <div class="right-block">
                                                    <h5 class="product-name"><a href="detail.html">Ipad</a></h5>
                                                    <div class="content_price">
                                                        <p class="price product-price">Retail price</p>
                                                        <p>$138,95</p>
                                                    </div>
                                                    <div class="discounted-percentage">-15%</div>
                                                    <div class="vcoin-wrapper vcoin-discounted-wrapper">
                                                        <span class="product-coin discounted-vcoin">84.15</span>
                                                        <span class="product-coin original-vcoin">99</span>
                                                    </div>
                                                </div>
                                                <div class="left-block">
                                                    <a href="detail.html">
                                                        <img class="img-responsive" alt="product"
                                                             src="assets/data/option6/p5.jpg"/></a>
                                                    <div class="add-to-cart">
                                                        <a title="Add to Cart" href="#">Add to Cart</a>
                                                    </div>
                                                </div>
                                            </li>
                                            <li class="col-sm-4">
                                                <div class="right-block">
                                                    <h5 class="product-name"><a href="detail.html">Sonic Camera</a></h5>
                                                    <div class="content_price">
                                                        <p class="price product-price">Retail price</p>
                                                        <p>$138,95</p>
                                                    </div>
                                                    <div class="vcoin-wrapper">
                                                        <span class="product-coin original-vcoin">99</span>
                                                    </div>
                                                </div>
                                                <div class="left-block">
                                                    <a href="detail.html">
                                                        <img class="img-responsive" alt="product"
                                                             src="assets/data/option6/p70.jpg"/></a>
                                                    <div class="add-to-cart">
                                                        <a title="Add to Cart" href="#">Add to Cart</a>
                                                    </div>
                                                </div>
                                            </li>
                                            <li class="col-sm-4">
                                                <div class="right-block">
                                                    <h5 class="product-name"><a href="detail.html">Blue Nano
                                                            Projector</a></h5>
                                                    <div class="content_price">
                                                        <p class="price product-price">Retail price</p>
                                                        <p>$138,95</p>
                                                    </div>
                                                    <div class="discounted-percentage">-15%</div>
                                                    <div class="vcoin-wrapper vcoin-discounted-wrapper">
                                                        <span class="product-coin discounted-vcoin">84.15</span>
                                                        <span class="product-coin original-vcoin">99</span>
                                                    </div>
                                                </div>
                                                <div class="left-block">
                                                    <a href="detail.html">
                                                        <img class="img-responsive" alt="product"
                                                             src="assets/data/option6/p71.jpg"/></a>
                                                    <div class="add-to-cart">
                                                        <a title="Add to Cart" href="#">Add to Cart</a>
                                                    </div>
                                                </div>
                                            </li>
                                            <li class="col-sm-4">
                                                <div class="right-block">
                                                    <h5 class="product-name"><a href="detail.html">Smartphone
                                                            Battery</a></h5>
                                                    <div class="content_price">
                                                        <p class="price product-price">Retail price</p>
                                                        <p>$138,95</p>
                                                    </div>
                                                    <div class="vcoin-wrapper">
                                                        <span class="product-coin original-vcoin">99</span>
                                                    </div>
                                                </div>
                                                <div class="left-block">
                                                    <a href="detail.html">
                                                        <img class="img-responsive" alt="product"
                                                             src="assets/data/option6/p5.jpg"/></a>
                                                    <div class="add-to-cart">
                                                        <a title="Add to Cart" href="#">Add to Cart</a>
                                                    </div>
                                                </div>
                                            </li>
                                            <li class="col-sm-4">
                                                <div class="right-block">
                                                    <h5 class="product-name"><a href="detail.html">Headphone &
                                                            earphone</a></h5>
                                                    <div class="content_price">
                                                        <p class="price product-price">Retail price</p>
                                                        <p>$138,95</p>
                                                    </div>
                                                    <div class="discounted-percentage">-15%</div>
                                                    <div class="vcoin-wrapper vcoin-discounted-wrapper">
                                                        <span class="product-coin discounted-vcoin">84.15</span>
                                                        <span class="product-coin original-vcoin">99</span>
                                                    </div>
                                                </div>
                                                <div class="left-block">
                                                    <a href="detail.html">
                                                        <img class="img-responsive" alt="product"
                                                             src="assets/data/option6/p6.jpg"/></a>
                                                    <div class="add-to-cart">
                                                        <a title="Add to Cart" href="#">Add to Cart</a>
                                                    </div>
                                                </div>
                                            </li>
                                            <li class="col-sm-4">
                                                <div class="right-block">
                                                    <h5 class="product-name"><a href="detail.html">SamSing Gallaxy
                                                            S5</a></h5>
                                                    <div class="content_price">
                                                        <p class="price product-price">Retail price</p>
                                                        <p>$138,95</p>
                                                    </div>
                                                    <div class="vcoin-wrapper">
                                                        <span class="product-coin original-vcoin">99</span>
                                                    </div>
                                                </div>
                                                <div class="left-block">
                                                    <a href="detail.html">
                                                        <img class="img-responsive" alt="product"
                                                             src="assets/data/option6/p11.jpg"/></a>
                                                    <div class="add-to-cart">
                                                        <a title="Add to Cart" href="#">Add to Cart</a>
                                                    </div>
                                                </div>
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                                <div class="tab-panel" id="tab-11">
                                    <div class="box-left">
                                        <div class="banner-img">
                                            <a href="detail.html"><img src="assets/data/option6/banner6.jpg"
                                                                       alt="Banner Product"></a>
                                        </div>
                                    </div>
                                    <div class="box-right">
                                        <ul class="product-list row">
                                            <li class="col-sm-4">
                                                <div class="right-block">
                                                    <h5 class="product-name"><a href="detail.html">Headphone &
                                                            earphone</a></h5>
                                                    <div class="content_price">
                                                        <p class="price product-price">Retail price</p>
                                                        <p>$138,95</p>
                                                    </div>
                                                    <div class="discounted-percentage">-15%</div>
                                                    <div class="vcoin-wrapper vcoin-discounted-wrapper">
                                                        <span class="product-coin discounted-vcoin">84.15</span>
                                                        <span class="product-coin original-vcoin">99</span>
                                                    </div>
                                                </div>
                                                <div class="left-block">
                                                    <a href="detail.html">
                                                        <img class="img-responsive" alt="product"
                                                             src="assets/data/option6/p73.jpg"/></a>
                                                    <div class="add-to-cart">
                                                        <a title="Add to Cart" href="#">Add to Cart</a>
                                                    </div>
                                                </div>
                                            </li>
                                            <li class="col-sm-4">
                                                <div class="right-block">
                                                    <h5 class="product-name"><a href="detail.html">Headphone &
                                                            earphone</a></h5>
                                                    <div class="content_price">
                                                        <p class="price product-price">Retail price</p>
                                                        <p>$138,95</p>
                                                    </div>
                                                    <div class="vcoin-wrapper">
                                                        <span class="product-coin original-vcoin">99</span>
                                                    </div>
                                                </div>
                                                <div class="left-block">
                                                    <a href="detail.html">
                                                        <img class="img-responsive" alt="product"
                                                             src="assets/data/option6/p72.jpg"/></a>
                                                    <div class="add-to-cart">
                                                        <a title="Add to Cart" href="#">Add to Cart</a>
                                                    </div>
                                                </div>
                                            </li>
                                            <li class="col-sm-4">
                                                <div class="right-block">
                                                    <h5 class="product-name"><a href="detail.html">Headphone &
                                                            earphone</a></h5>
                                                    <div class="content_price">
                                                        <p class="price product-price">Retail price</p>
                                                        <p>$138,95</p>
                                                    </div>
                                                    <div class="discounted-percentage">-15%</div>
                                                    <div class="vcoin-wrapper vcoin-discounted-wrapper">
                                                        <span class="product-coin discounted-vcoin">84.15</span>
                                                        <span class="product-coin original-vcoin">99</span>
                                                    </div>
                                                </div>
                                                <div class="left-block">
                                                    <a href="detail.html">
                                                        <img class="img-responsive" alt="product"
                                                             src="assets/data/option6/p71.jpg"/></a>
                                                    <div class="add-to-cart">
                                                        <a title="Add to Cart" href="#">Add to Cart</a>
                                                    </div>
                                                </div>
                                            </li>
                                            <li class="col-sm-4">
                                                <div class="right-block">
                                                    <h5 class="product-name"><a href="detail.html">Headphone &
                                                            earphone</a></h5>
                                                    <div class="content_price">
                                                        <p class="price product-price">Retail price</p>
                                                        <p>$138,95</p>
                                                    </div>
                                                    <div class="vcoin-wrapper">
                                                        <span class="product-coin original-vcoin">99</span>
                                                    </div>
                                                </div>
                                                <div class="left-block">
                                                    <a href="detail.html">
                                                        <img class="img-responsive" alt="product"
                                                             src="assets/data/option6/p70.jpg"/></a>
                                                    <div class="add-to-cart">
                                                        <a title="Add to Cart" href="#">Add to Cart</a>
                                                    </div>
                                                </div>
                                            </li>
                                            <li class="col-sm-4">
                                                <div class="right-block">
                                                    <h5 class="product-name"><a href="detail.html">Headphone &
                                                            earphone</a></h5>
                                                    <div class="content_price">
                                                        <p class="price product-price">Retail price</p>
                                                        <p>$138,95</p>
                                                    </div>
                                                    <div class="discounted-percentage">-15%</div>
                                                    <div class="vcoin-wrapper vcoin-discounted-wrapper">
                                                        <span class="product-coin discounted-vcoin">84.15</span>
                                                        <span class="product-coin original-vcoin">99</span>
                                                    </div>
                                                </div>
                                                <div class="left-block">
                                                    <a href="detail.html">
                                                        <img class="img-responsive" alt="product"
                                                             src="assets/data/option6/p69.jpg"/></a>
                                                    <div class="add-to-cart">
                                                        <a title="Add to Cart" href="#">Add to Cart</a>
                                                    </div>
                                                </div>
                                            </li>
                                            <li class="col-sm-4">
                                                <div class="right-block">
                                                    <h5 class="product-name"><a href="detail.html">Headphone &
                                                            earphone</a></h5>
                                                    <div class="content_price">
                                                        <p class="price product-price">Retail price</p>
                                                        <p>$138,95</p>
                                                    </div>
                                                    <div class="vcoin-wrapper">
                                                        <span class="product-coin original-vcoin">99</span>
                                                    </div>
                                                </div>
                                                <div class="left-block">
                                                    <a href="detail.html">
                                                        <img class="img-responsive" alt="product"
                                                             src="assets/data/option6/p68.jpg"/></a>
                                                    <div class="add-to-cart">
                                                        <a title="Add to Cart" href="#">Add to Cart</a>
                                                    </div>
                                                </div>
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- end featured category Digital-->
        <!-- featured category furniture -->
        <div class="category-featured furniture">
            <nav class="navbar nav-menu show-brand">
                <div class="container">
                    <!-- Brand and toggle get grouped for better mobile display -->
                    <div class="navbar-brand"><a href="#"><img alt="fashion" src="assets/data/icon-furniture.png"/>furniture</a>
                    </div>
                    <span class="toggle-menu"></span>
                    <!-- Collect the nav links, forms, and other content for toggling -->
                    <div class="collapse navbar-collapse">
                        <ul class="nav navbar-nav">
                            <li class="active"><a data-toggle="tab" href="#tab-12">Best salers</a></li>
                            <li><a data-toggle="tab" href="#tab-13">Specials</a></li>
                            <li><a data-toggle="tab" href="#tab-12">New Arrivals</a></li>
                            <li><a data-toggle="tab" href="#tab-13">Most Reviews</a></li>
                        </ul>
                    </div><!-- /.navbar-collapse -->
                </div><!-- /.container-fluid -->
                <div id="elevator-5" class="floor-elevator">
                    <a href="#elevator-4" class="btn-elevator up fa fa-angle-up"></a>
                    <a href="#elevator-6" class="btn-elevator down fa fa-angle-down"></a>
                </div>
            </nav>
            <div class="product-featured clearfix">
                <div class="row">
                    <div class="col-sm-2 sub-category-wapper">
                        <ul class="sub-category-list">
                            <li><a href="#">Cross Stitch</a></li>
                            <li><a href="#">Diamond Painting</a></li>
                            <li><a href="#">Cake Tools</a></li>
                            <li><a href="#">Cooking Tools</a></li>
                            <li><a href="#">Curtain</a></li>
                            <li><a href="#">Bedding</a></li>
                            <li><a href="#">Wall Décor</a></li>
                            <li><a href="#">Decorative Flowers</a></li>
                            <li><a href="#">Home Storage</a></li>
                            <li><a href="#">Pest Control</a></li>
                            <li><a href="#">Pet Products</a></li>
                        </ul>
                    </div>
                    <div class="col-sm-10 col-right-tab">
                        <div class="product-featured-tab-content">
                            <div class="tab-container">
                                <div class="tab-panel active" id="tab-12">
                                    <div class="box-left">
                                        <div class="banner-img">
                                            <a href="#"><img src="assets/data/option6/banner7.jpg" alt="Banner Product"></a>
                                        </div>
                                    </div>
                                    <div class="box-right">
                                        <ul class="product-list row">
                                            <li class="col-sm-4">
                                                <div class="right-block">
                                                    <h5 class="product-name"><a href="detail.html">Interesting Chair</a>
                                                    </h5>
                                                    <div class="content_price">
                                                        <p class="price product-price">Retail price</p>
                                                        <p>$138,95</p>
                                                    </div>
                                                    <div class="discounted-percentage">-15%</div>
                                                    <div class="vcoin-wrapper vcoin-discounted-wrapper">
                                                        <span class="product-coin discounted-vcoin">84.15</span>
                                                        <span class="product-coin original-vcoin">99</span>
                                                    </div>
                                                </div>
                                                <div class="left-block">
                                                    <a href="detail.html">
                                                        <img class="img-responsive" alt="product"
                                                             src="assets/data/option6/p8.jpg"/></a>
                                                    <div class="add-to-cart">
                                                        <a title="Add to Cart" href="#">Add to Cart</a>
                                                    </div>
                                                </div>
                                            </li>
                                            <li class="col-sm-4">
                                                <div class="right-block">
                                                    <h5 class="product-name"><a href="detail.html">Relaxed Chair</a>
                                                    </h5>
                                                    <div class="content_price">
                                                        <p class="price product-price">Retail price</p>
                                                        <p>$138,95</p>
                                                    </div>
                                                    <div class="vcoin-wrapper">
                                                        <span class="product-coin original-vcoin">99</span>
                                                    </div>
                                                </div>
                                                <div class="left-block">
                                                    <a href="detail.html">
                                                        <img class="img-responsive" alt="product"
                                                             src="assets/data/option6/p9.jpg"/></a>
                                                    <div class="add-to-cart">
                                                        <a title="Add to Cart" href="#">Add to Cart</a>
                                                    </div>
                                                </div>
                                            </li>
                                            <li class="col-sm-4">
                                                <div class="right-block">
                                                    <h5 class="product-name"><a href="detail.html">Innovative Red
                                                            Chair</a></h5>
                                                    <div class="content_price">
                                                        <p class="price product-price">Retail price</p>
                                                        <p>$138,95</p>
                                                    </div>
                                                    <div class="discounted-percentage">-15%</div>
                                                    <div class="vcoin-wrapper vcoin-discounted-wrapper">
                                                        <span class="product-coin discounted-vcoin">84.15</span>
                                                        <span class="product-coin original-vcoin">99</span>
                                                    </div>
                                                </div>
                                                <div class="left-block">
                                                    <a href="detail.html">
                                                        <img class="img-responsive" alt="product"
                                                             src="assets/data/option6/p7.jpg"/></a>
                                                    <div class="add-to-cart">
                                                        <a title="Add to Cart" href="#">Add to Cart</a>
                                                    </div>
                                                </div>
                                            </li>
                                            <li class="col-sm-4">
                                                <div class="right-block">
                                                    <h5 class="product-name"><a href="detail.html">White Stack Chair</a>
                                                    </h5>
                                                    <div class="content_price">
                                                        <p class="price product-price">Retail price</p>
                                                        <p>$138,95</p>
                                                    </div>
                                                    <div class="vcoin-wrapper">
                                                        <span class="product-coin original-vcoin">99</span>
                                                    </div>
                                                </div>
                                                <div class="left-block">
                                                    <a href="detail.html">
                                                        <img class="img-responsive" alt="product"
                                                             src="assets/data/option6/p14.jpg"/></a>
                                                    <div class="add-to-cart">
                                                        <a title="Add to Cart" href="#">Add to Cart</a>
                                                    </div>
                                                </div>
                                            </li>
                                            <li class="col-sm-4">
                                                <div class="right-block">
                                                    <h5 class="product-name"><a href="detail.html">Kute Innovative
                                                            Chair</a></h5>
                                                    <div class="content_price">
                                                        <p class="price product-price">Retail price</p>
                                                        <p>$138,95</p>
                                                    </div>
                                                    <div class="discounted-percentage">-15%</div>
                                                    <div class="vcoin-wrapper vcoin-discounted-wrapper">
                                                        <span class="product-coin discounted-vcoin">84.15</span>
                                                        <span class="product-coin original-vcoin">99</span>
                                                    </div>
                                                </div>
                                                <div class="left-block">
                                                    <a href="detail.html">
                                                        <img class="img-responsive" alt="product"
                                                             src="assets/data/option6/p12.jpg"/></a>
                                                    <div class="add-to-cart">
                                                        <a title="Add to Cart" href="#">Add to Cart</a>
                                                    </div>
                                                </div>
                                            </li>
                                            <li class="col-sm-4">
                                                <div class="right-block">
                                                    <h5 class="product-name"><a href="detail.html">Pretty Green &
                                                            Inox</a></h5>
                                                    <div class="content_price">
                                                        <p class="price product-price">Retail price</p>
                                                        <p>$138,95</p>
                                                    </div>
                                                    <div class="vcoin-wrapper">
                                                        <span class="product-coin original-vcoin">99</span>
                                                    </div>
                                                </div>
                                                <div class="left-block">
                                                    <a href="detail.html">
                                                        <img class="img-responsive" alt="product"
                                                             src="assets/data/option6/p13.jpg"/></a>
                                                    <div class="add-to-cart">
                                                        <a title="Add to Cart" href="#">Add to Cart</a>
                                                    </div>
                                                </div>
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                                <div class="tab-panel" id="tab-13">
                                    <div class="box-left">
                                        <div class="banner-img">
                                            <a href="#"><img src="assets/data/option6/banner-product3.jpg"
                                                             alt="Banner Product"></a>
                                        </div>
                                    </div>
                                    <div class="box-right">
                                        <ul class="product-list row">
                                            <li class="col-sm-4">
                                                <div class="right-block">
                                                    <h5 class="product-name"><a href="detail.html">Headphone &
                                                            earphone</a></h5>
                                                    <div class="content_price">
                                                        <p class="price product-price">Retail price</p>
                                                        <p>$138,95</p>
                                                    </div>
                                                    <div class="discounted-percentage">-15%</div>
                                                    <div class="vcoin-wrapper vcoin-discounted-wrapper">
                                                        <span class="product-coin discounted-vcoin">84.15</span>
                                                        <span class="product-coin original-vcoin">99</span>
                                                    </div>
                                                </div>
                                                <div class="left-block">
                                                    <a href="detail.html">
                                                        <img class="img-responsive" alt="product"
                                                             src="assets/data/option6/p78.jpg"/></a>
                                                    <div class="add-to-cart">
                                                        <a title="Add to Cart" href="#">Add to Cart</a>
                                                    </div>
                                                </div>
                                            </li>
                                            <li class="col-sm-4">
                                                <div class="right-block">
                                                    <h5 class="product-name"><a href="detail.html">Headphone &
                                                            earphone</a></h5>
                                                    <div class="content_price">
                                                        <p class="price product-price">Retail price</p>
                                                        <p>$138,95</p>
                                                    </div>
                                                    <div class="vcoin-wrapper">
                                                        <span class="product-coin original-vcoin">99</span>
                                                    </div>
                                                </div>
                                                <div class="left-block">
                                                    <a href="detail.html">
                                                        <img class="img-responsive" alt="product"
                                                             src="assets/data/option6/p77.jpg"/></a>
                                                    <div class="add-to-cart">
                                                        <a title="Add to Cart" href="#">Add to Cart</a>
                                                    </div>
                                                </div>
                                            </li>
                                            <li class="col-sm-4">
                                                <div class="right-block">
                                                    <h5 class="product-name"><a href="detail.html">Headphone &
                                                            earphone</a></h5>
                                                    <div class="content_price">
                                                        <p class="price product-price">Retail price</p>
                                                        <p>$138,95</p>
                                                    </div>
                                                    <div class="discounted-percentage">-15%</div>
                                                    <div class="vcoin-wrapper vcoin-discounted-wrapper">
                                                        <span class="product-coin discounted-vcoin">84.15</span>
                                                        <span class="product-coin original-vcoin">99</span>
                                                    </div>
                                                </div>
                                                <div class="left-block">
                                                    <a href="detail.html">
                                                        <img class="img-responsive" alt="product"
                                                             src="assets/data/option6/p78.jpg"/></a>
                                                    <div class="add-to-cart">
                                                        <a title="Add to Cart" href="#">Add to Cart</a>
                                                    </div>
                                                </div>
                                            </li>
                                            <li class="col-sm-4">
                                                <div class="right-block">
                                                    <h5 class="product-name"><a href="detail.html">Headphone &
                                                            earphone</a></h5>
                                                    <div class="content_price">
                                                        <p class="price product-price">Retail price</p>
                                                        <p>$138,95</p>
                                                    </div>
                                                    <div class="vcoin-wrapper">
                                                        <span class="product-coin original-vcoin">99</span>
                                                    </div>
                                                </div>
                                                <div class="left-block">
                                                    <a href="detail.html">
                                                        <img class="img-responsive" alt="product"
                                                             src="assets/data/option6/p76.jpg"/></a>
                                                    <div class="add-to-cart">
                                                        <a title="Add to Cart" href="#">Add to Cart</a>
                                                    </div>
                                                </div>
                                            </li>
                                            <li class="col-sm-4">
                                                <div class="right-block">
                                                    <h5 class="product-name"><a href="detail.html">Headphone &
                                                            earphone</a></h5>
                                                    <div class="content_price">
                                                        <p class="price product-price">Retail price</p>
                                                        <p>$138,95</p>
                                                    </div>
                                                    <div class="discounted-percentage">-15%</div>
                                                    <div class="vcoin-wrapper vcoin-discounted-wrapper">
                                                        <span class="product-coin discounted-vcoin">84.15</span>
                                                        <span class="product-coin original-vcoin">99</span>
                                                    </div>
                                                </div>
                                                <div class="left-block">
                                                    <a href="detail.html">
                                                        <img class="img-responsive" alt="product"
                                                             src="assets/data/option6/p75.jpg"/></a>
                                                    <div class="add-to-cart">
                                                        <a title="Add to Cart" href="#">Add to Cart</a>
                                                    </div>
                                                </div>
                                            </li>
                                            <li class="col-sm-4">
                                                <div class="right-block">
                                                    <h5 class="product-name"><a href="detail.html">Headphone &
                                                            earphone</a></h5>
                                                    <div class="content_price">
                                                        <p class="price product-price">Retail price</p>
                                                        <p>$138,95</p>
                                                    </div>
                                                    <div class="vcoin-wrapper">
                                                        <span class="product-coin original-vcoin">99</span>
                                                    </div>
                                                </div>
                                                <div class="left-block">
                                                    <a href="detail.html">
                                                        <img class="img-responsive" alt="product"
                                                             src="assets/data/option6/p74.jpg"/></a>
                                                    <div class="add-to-cart">
                                                        <a title="Add to Cart" href="#">Add to Cart</a>
                                                    </div>
                                                </div>
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- end featured category furniture-->
        <!-- featured category jewelry -->
        <div class="category-featured jewelry">
            <nav class="navbar nav-menu show-brand">
                <div class="container">
                    <!-- Brand and toggle get grouped for better mobile display -->
                    <div class="navbar-brand"><a href="#"><img alt="fashion" src="assets/data/icon-jewelry.png"/>jewelry</a>
                    </div>
                    <span class="toggle-menu"></span>
                    <!-- Collect the nav links, forms, and other content for toggling -->
                    <div class="collapse navbar-collapse">
                        <ul class="nav navbar-nav">
                            <li class="active"><a data-toggle="tab" href="#tab-14">Best salers</a></li>
                            <li><a data-toggle="tab" href="#tab-15">Specials</a></li>
                            <li><a data-toggle="tab" href="#tab-14">New Arrivals</a></li>
                            <li><a data-toggle="tab" href="#tab-15">Most Reviews</a></li>
                        </ul>
                    </div><!-- /.navbar-collapse -->
                </div><!-- /.container-fluid -->
                <div id="elevator-6" class="floor-elevator">
                    <a href="#elevator-5" class="btn-elevator up fa fa-angle-up"></a>
                    <a href="#" class="btn-elevator disabled down fa fa-angle-down"></a>
                </div>
            </nav>
            <div class="product-featured clearfix">
                <div class="row">
                    <div class="col-sm-2 sub-category-wapper">
                        <ul class="sub-category-list">
                            <li><a href="#">Multi-Layer Necklaces</a></li>
                            <li><a href="#">925 Silver</a></li>
                            <li><a href="#">Pearl Jewelry</a></li>
                            <li><a href="#">Friendship Bracelets</a></li>
                            <li><a href="#">Brinco</a></li>
                            <li><a href="#">Body Chains</a></li>
                            <li><a href="#">Carp Fishing</a></li>
                            <li><a href="#">DIY Beads</a></li>
                            <li><a href="#">Digital Watches</a></li>
                            <li><a href="#">Dress Watches</a></li>
                            <li><a href="#">Men's Watches</a></li>
                        </ul>
                    </div>
                    <div class="col-sm-10 col-right-tab">
                        <div class="product-featured-tab-content">
                            <div class="tab-container">
                                <div class="tab-panel active" id="tab-12">
                                    <div class="box-left">
                                        <div class="banner-img">
                                            <a href="#"><img src="assets/data/option6/banner8.jpg" alt="Banner Product"></a>
                                        </div>
                                    </div>
                                    <div class="box-right">
                                        <ul class="product-list row">
                                            <li class="col-sm-4">
                                                <div class="right-block">
                                                    <h5 class="product-name"><a href="detail.html">Interesting Chair</a>
                                                    </h5>
                                                    <div class="content_price">
                                                        <p class="price product-price">Retail price</p>
                                                        <p>$138,95</p>
                                                    </div>
                                                    <div class="discounted-percentage">-15%</div>
                                                    <div class="vcoin-wrapper vcoin-discounted-wrapper">
                                                        <span class="product-coin discounted-vcoin">84.15</span>
                                                        <span class="product-coin original-vcoin">99</span>
                                                    </div>
                                                </div>
                                                <div class="left-block">
                                                    <a href="detail.html">
                                                        <img class="img-responsive" alt="product"
                                                             src="assets/data/option6/p81.jpg"/></a>
                                                    <div class="add-to-cart">
                                                        <a title="Add to Cart" href="#">Add to Cart</a>
                                                    </div>
                                                </div>
                                            </li>
                                            <li class="col-sm-4">
                                                <div class="right-block">
                                                    <h5 class="product-name"><a href="detail.html">Relaxed Chair</a>
                                                    </h5>
                                                    <div class="content_price">
                                                        <p class="price product-price">Retail price</p>
                                                        <p>$138,95</p>
                                                    </div>
                                                    <div class="vcoin-wrapper">
                                                        <span class="product-coin original-vcoin">99</span>
                                                    </div>
                                                </div>
                                                <div class="left-block">
                                                    <a href="detail.html">
                                                        <img class="img-responsive" alt="product"
                                                             src="assets/data/option6/p82.jpg"/></a>
                                                    <div class="add-to-cart">
                                                        <a title="Add to Cart" href="#">Add to Cart</a>
                                                    </div>
                                                </div>
                                            </li>
                                            <li class="col-sm-4">
                                                <div class="right-block">
                                                    <h5 class="product-name"><a href="detail.html">Innovative Red
                                                            Chair</a></h5>
                                                    <div class="content_price">
                                                        <p class="price product-price">Retail price</p>
                                                        <p>$138,95</p>
                                                    </div>
                                                    <div class="discounted-percentage">-15%</div>
                                                    <div class="vcoin-wrapper vcoin-discounted-wrapper">
                                                        <span class="product-coin discounted-vcoin">84.15</span>
                                                        <span class="product-coin original-vcoin">99</span>
                                                    </div>
                                                </div>
                                                <div class="left-block">
                                                    <a href="detail.html">
                                                        <img class="img-responsive" alt="product"
                                                             src="assets/data/option6/p83.jpg"/></a>
                                                    <div class="add-to-cart">
                                                        <a title="Add to Cart" href="#">Add to Cart</a>
                                                    </div>
                                                </div>
                                            </li>
                                            <li class="col-sm-4">
                                                <div class="right-block">
                                                    <h5 class="product-name"><a href="detail.html">White Stack Chair</a>
                                                    </h5>
                                                    <div class="content_price">
                                                        <p class="price product-price">Retail price</p>
                                                        <p>$138,95</p>
                                                    </div>
                                                    <div class="vcoin-wrapper">
                                                        <span class="product-coin original-vcoin">99</span>
                                                    </div>
                                                </div>
                                                <div class="left-block">
                                                    <a href="detail.html">
                                                        <img class="img-responsive" alt="product"
                                                             src="assets/data/option6/p84.jpg"/></a>
                                                    <div class="add-to-cart">
                                                        <a title="Add to Cart" href="#">Add to Cart</a>
                                                    </div>
                                                </div>
                                            </li>
                                            <li class="col-sm-4">
                                                <div class="right-block">
                                                    <h5 class="product-name"><a href="detail.html">Kute Innovative
                                                            Chair</a></h5>
                                                    <div class="content_price">
                                                        <p class="price product-price">Retail price</p>
                                                        <p>$138,95</p>
                                                    </div>
                                                    <div class="discounted-percentage">-15%</div>
                                                    <div class="vcoin-wrapper vcoin-discounted-wrapper">
                                                        <span class="product-coin discounted-vcoin">84.15</span>
                                                        <span class="product-coin original-vcoin">99</span>
                                                    </div>
                                                </div>
                                                <div class="left-block">
                                                    <a href="detail.html">
                                                        <img class="img-responsive" alt="product"
                                                             src="assets/data/option6/p85.jpg"/></a>
                                                    <div class="add-to-cart">
                                                        <a title="Add to Cart" href="#">Add to Cart</a>
                                                    </div>
                                                </div>
                                            </li>
                                            <li class="col-sm-4">
                                                <div class="right-block">
                                                    <h5 class="product-name"><a href="detail.html">Pretty Green &
                                                            Inox</a></h5>
                                                    <div class="content_price">
                                                        <p class="price product-price">Retail price</p>
                                                        <p>$138,95</p>
                                                    </div>
                                                    <div class="vcoin-wrapper">
                                                        <span class="product-coin original-vcoin">99</span>
                                                    </div>
                                                </div>
                                                <div class="left-block">
                                                    <a href="detail.html">
                                                        <img class="img-responsive" alt="product"
                                                             src="assets/data/option6/p86.jpg"/></a>
                                                    <div class="add-to-cart">
                                                        <a title="Add to Cart" href="#">Add to Cart</a>
                                                    </div>
                                                </div>
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                                <div class="tab-panel" id="tab-13">
                                    <div class="box-left">
                                        <div class="banner-img">
                                            <a href="#"><img src="assets/data/option6/banner8.jpg" alt="Banner Product"></a>
                                        </div>
                                    </div>
                                    <div class="box-right">
                                        <ul class="product-list row">
                                            <li class="col-sm-4">
                                                <div class="right-block">
                                                    <h5 class="product-name"><a href="detail.html">Headphone &
                                                            earphone</a></h5>
                                                    <div class="content_price">
                                                        <p class="price product-price">Retail price</p>
                                                        <p>$138,95</p>
                                                    </div>
                                                    <div class="discounted-percentage">-15%</div>
                                                    <div class="vcoin-wrapper vcoin-discounted-wrapper">
                                                        <span class="product-coin discounted-vcoin">84.15</span>
                                                        <span class="product-coin original-vcoin">99</span>
                                                    </div>
                                                </div>
                                                <div class="left-block">
                                                    <a href="detail.html">
                                                        <img class="img-responsive" alt="product"
                                                             src="assets/data/option6/p86.jpg"/></a>
                                                    <div class="add-to-cart">
                                                        <a title="Add to Cart" href="#">Add to Cart</a>
                                                    </div>
                                                </div>
                                            </li>
                                            <li class="col-sm-4">
                                                <div class="right-block">
                                                    <h5 class="product-name"><a href="detail.html">Headphone &
                                                            earphone</a></h5>
                                                    <div class="content_price">
                                                        <p class="price product-price">Retail price</p>
                                                        <p>$138,95</p>
                                                    </div>
                                                    <div class="vcoin-wrapper">
                                                        <span class="product-coin original-vcoin">99</span>
                                                    </div>
                                                </div>
                                                <div class="left-block">
                                                    <a href="detail.html">
                                                        <img class="img-responsive" alt="product"
                                                             src="assets/data/option6/p85.jpg"/></a>
                                                    <div class="add-to-cart">
                                                        <a title="Add to Cart" href="#">Add to Cart</a>
                                                    </div>
                                                </div>
                                            </li>
                                            <li class="col-sm-4">
                                                <div class="right-block">
                                                    <h5 class="product-name"><a href="detail.html">Headphone &
                                                            earphone</a></h5>
                                                    <div class="content_price">
                                                        <p class="price product-price">Retail price</p>
                                                        <p>$138,95</p>
                                                    </div>
                                                    <div class="discounted-percentage">-15%</div>
                                                    <div class="vcoin-wrapper vcoin-discounted-wrapper">
                                                        <span class="product-coin discounted-vcoin">84.15</span>
                                                        <span class="product-coin original-vcoin">99</span>
                                                    </div>
                                                </div>
                                                <div class="left-block">
                                                    <a href="detail.html">
                                                        <img class="img-responsive" alt="product"
                                                             src="assets/data/option6/p84.jpg"/></a>
                                                    <div class="add-to-cart">
                                                        <a title="Add to Cart" href="#">Add to Cart</a>
                                                    </div>
                                                </div>
                                            </li>
                                            <li class="col-sm-4">
                                                <div class="right-block">
                                                    <h5 class="product-name"><a href="detail.html">Headphone &
                                                            earphone</a></h5>
                                                    <div class="content_price">
                                                        <p class="price product-price">Retail price</p>
                                                        <p>$138,95</p>
                                                    </div>
                                                    <div class="vcoin-wrapper">
                                                        <span class="product-coin original-vcoin">99</span>
                                                    </div>
                                                </div>
                                                <div class="left-block">
                                                    <a href="detail.html">
                                                        <img class="img-responsive" alt="product"
                                                             src="assets/data/option6/p83.jpg"/></a>
                                                    <div class="add-to-cart">
                                                        <a title="Add to Cart" href="#">Add to Cart</a>
                                                    </div>
                                                </div>
                                            </li>
                                            <li class="col-sm-4">
                                                <div class="right-block">
                                                    <h5 class="product-name"><a href="detail.html">Headphone &
                                                            earphone</a></h5>
                                                    <div class="content_price">
                                                        <p class="price product-price">Retail price</p>
                                                        <p>$138,95</p>
                                                    </div>
                                                    <div class="discounted-percentage">-15%</div>
                                                    <div class="vcoin-wrapper vcoin-discounted-wrapper">
                                                        <span class="product-coin discounted-vcoin">84.15</span>
                                                        <span class="product-coin original-vcoin">99</span>
                                                    </div>
                                                </div>
                                                <div class="left-block">
                                                    <a href="detail.html">
                                                        <img class="img-responsive" alt="product"
                                                             src="assets/data/option6/p82.jpg"/></a>
                                                    <div class="add-to-cart">
                                                        <a title="Add to Cart" href="#">Add to Cart</a>
                                                    </div>
                                                </div>
                                            </li>
                                            <li class="col-sm-4">
                                                <div class="right-block">
                                                    <h5 class="product-name"><a href="detail.html">Headphone &
                                                            earphone</a></h5>
                                                    <div class="content_price">
                                                        <p class="price product-price">Retail price</p>
                                                        <p>$138,95</p>
                                                    </div>
                                                    <div class="vcoin-wrapper">
                                                        <span class="product-coin original-vcoin">99</span>
                                                    </div>
                                                </div>
                                                <div class="left-block">
                                                    <a href="detail.html">
                                                        <img class="img-responsive" alt="product"
                                                             src="assets/data/option6/p81.jpg"/></a>
                                                    <div class="add-to-cart">
                                                        <a title="Add to Cart" href="#">Add to Cart</a>
                                                    </div>
                                                </div>
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- end featured category jewelry-->
    </div>
</div>

<div id="content-wrap">
    <div class="container">
        <div class="band-logo owl-carousel" data-dots="false" data-loop="true" data-nav="true" data-margin="1.2"
             data-autoplay="true" data-responsive='{"0":{"items":3},"600":{"items":5},"1000":{"items":8}}'>
            <a href="#"><img src="assets/data/option6/band-logo.png" alt="Band logo"></a>
            <a href="#"><img src="assets/data/option6/band-logo.png" alt="Band logo"></a>
            <a href="#"><img src="assets/data/option6/band-logo.png" alt="Band logo"></a>
            <a href="#"><img src="assets/data/option6/band-logo.png" alt="Band logo"></a>
            <a href="#"><img src="assets/data/option6/band-logo.png" alt="Band logo"></a>
            <a href="#"><img src="assets/data/option6/band-logo.png" alt="Band logo"></a>
            <a href="#"><img src="assets/data/option6/band-logo.png" alt="Band logo"></a>
            <a href="#"><img src="assets/data/option6/band-logo.png" alt="Band logo"></a>
        </div>
        <!-- service 2 -->
        <div class="services2">
            <ul>
                <li class="col-xs-6 col-sm-6 col-md-4 services2-item">
                    <div class="service-wapper">
                        <div class="row">
                            <div class="col-sm-6 image">
                                <div class="icon">
                                    <img src="assets/data/icon-s1.png" alt="service">
                                </div>
                                <h3 class="title"><a href="#">Great Value</a></h3>
                            </div>
                            <div class="col-sm-6 text">
                                We offer competitive prices on our 100 million plus product range.
                            </div>
                        </div>
                    </div>
                </li>
                <li class="col-xs-6 col-sm-6 col-md-4 services2-item">
                    <div class="service-wapper">
                        <div class="row">
                            <div class="col-sm-6 image">
                                <div class="icon">
                                    <img src="assets/data/icon-s2.png" alt="service">
                                </div>
                                <h3 class="title"><a href="#">Worldwide Delivery</a></h3>
                            </div>
                            <div class="col-sm-6 text">
                                With sites in 5 languages, we ship to over 200 countries & regions.
                            </div>
                        </div>
                    </div>
                </li>
                <li class="col-xs-6 col-sm-6 col-md-4 services2-item">
                    <div class="service-wapper">
                        <div class="row">
                            <div class="col-sm-6 image">
                                <div class="icon">
                                    <img src="assets/data/icon-s3.png" alt="service">
                                </div>
                                <h3 class="title"><a href="#">Safe Payment</a></h3>
                            </div>
                            <div class="col-sm-6 text">
                                Pay with the world’s most popular and secure payment methods.
                            </div>
                        </div>
                    </div>
                </li>
                <li class="col-xs-6 col-sm-6 col-md-4 services2-item">
                    <div class="service-wapper">
                        <div class="row">
                            <div class="col-sm-6 image">
                                <div class="icon">
                                    <img src="assets/data/icon-s4.png" alt="service">
                                </div>
                                <h3 class="title"><a href="#">Shop with Confidence</a></h3>
                            </div>
                            <div class="col-sm-6 text">
                                Our Buyer Protection covers your purchase from click to delivery.
                            </div>
                        </div>
                    </div>
                </li>
                <li class="col-xs-6 col-sm-6 col-md-4 services2-item">
                    <div class="service-wapper">
                        <div class="row">
                            <div class="col-sm-6 image">
                                <div class="icon">
                                    <img src="assets/data/icon-s5.png" alt="service">
                                </div>
                                <h3 class="title"><a href="#">24/7 Help Center</a></h3>
                            </div>
                            <div class="col-sm-6 text">
                                Round-the-clock assistance for a smooth shopping experience.
                            </div>
                        </div>
                    </div>
                </li>
                <li class="col-xs-6 col-sm-6 col-md-4 services2-item">
                    <div class="service-wapper">
                        <div class="row">
                            <div class="col-sm-6 image">
                                <div class="icon">
                                    <img src="assets/data/icon-s6.png" alt="service">
                                </div>
                                <h3 class="title"><a href="#">Shop On-The-Go</a></h3>
                            </div>
                            <div class="col-sm-6 text">
                                Download the app and get the world of KuteShop at your fingertips.
                            </div>
                        </div>
                    </div>
                </li>

            </ul>
        </div>
        <!-- ./service 2 -->
    </div> <!-- /.container -->
</div>
<?php include("includes/footer.php"); ?>
<?php include("includes/scripts.php"); ?>
</body>
</html>