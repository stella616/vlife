<!-- HEADER -->
<div id="header" class="header">
    <div class="top-header hide-mobile">
        <div class="container">
            <div class="language ">
                <div class="dropdown">
                    <a class="current-open" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" href="#">English</a>
                    <ul class="dropdown-menu" role="menu">
                        <li><a href="#">English</a></li>
                        <li><a href="#">中文</a></li>
                    </ul>
                </div>
            </div>
            <div class="top-bar-social">
                <a href="#"><i class="fa fa-facebook"></i></a>
                <a href="#"><i class="fa fa-twitter"></i></a>
                <a href="#"><i class="fa fa-pinterest"></i></a>
                <a href="#"><i class="fa fa-google-plus"></i></a>
            </div>
            <div class="support-link">
                <a href="about.php">About Us</a>
            </div>

            <div id="user-info-top" class="user-info pull-right">
                <div class="dropdown">
                    <a href="login.php"><i class="fa fa-user"></i><span>Login / Register</span></a>
                </div>
            </div>
        </div>
    </div>
    <!--/.top-header -->
    <!-- MAIN HEADER -->
    <div class="container main-header">
        <div class="row">
            <div class="col-xs-12 col-sm-12 col-md-3 logo">
                <a href="index.php"><img alt="Vlife" src="assets/data/option6/logo.png"/></a>
            </div>
            <div class="mobile-nav">
                <div>
                    <a id="mobile-nav-menu"><i class="fa fa-bars"></i></a>
                </div>
                <div>
                    <a href="login.php"><i class="fa fa-user"></i></a>
                </div>
                <div>
                    <a id="mobile-btn-language"><i class="fa fa-language"></i></a>
                </div>
                <div>
                    <a href="order.php" id="icon-cart">
                        <i class="fa fa-shopping-cart"></i>
                        <span class="notify notify-right">2</span>
                    </a>
                </div>
            </div>
            <div id="mobile-navbar">
                <ul class="nav navbar-nav">
                    <li class="active">
                        <a href="index.php">Home</a>
                    </li>
                    <li class="dropdown">
                        <a href="category.php" class="dropdown-toggle" data-toggle="dropdown">Fashion</a>
                        <ul class="dropdown-menu mega_dropdown" role="menu" style="width: 830px;">
                            <li class="block-container col-sm-3">
                                <ul class="block">
                                    <li class="link_container group_header">
                                        <a href="#">MEN'S</a>
                                    </li>
                                    <li class="link_container"><a href="#">Skirts</a></li>
                                    <li class="link_container"><a href="#">Jackets</a></li>
                                    <li class="link_container"><a href="#">Tops</a></li>
                                    <li class="link_container"><a href="#">Scarves</a></li>
                                    <li class="link_container"><a href="#">Pants</a></li>
                                </ul>
                            </li>
                            <li class="block-container col-sm-3">
                                <ul class="block">
                                    <li class="link_container group_header">
                                        <a href="#">WOMEN'S</a>
                                    </li>
                                    <li class="link_container"><a href="#">Skirts</a></li>
                                    <li class="link_container"><a href="#">Jackets</a></li>
                                    <li class="link_container"><a href="#">Tops</a></li>
                                    <li class="link_container"><a href="#">Scarves</a></li>
                                    <li class="link_container"><a href="#">Pants</a></li>
                                </ul>
                            </li>
                            <li class="block-container col-sm-3">
                                <ul class="block">
                                    <li class="link_container group_header">
                                        <a href="#">Kids</a>
                                    </li>
                                    <li class="link_container"><a href="#">Shoes</a></li>
                                    <li class="link_container"><a href="#">Clothing</a></li>
                                    <li class="link_container"><a href="#">Tops</a></li>
                                    <li class="link_container"><a href="#">Scarves</a></li>
                                    <li class="link_container"><a href="#">Accessories</a></li>
                                </ul>
                            </li>
                            <li class="block-container col-sm-3">
                                <ul class="block">
                                    <li class="link_container group_header">
                                        <a href="#">TRENDING</a>
                                    </li>
                                    <li class="link_container"><a href="#">Men's Clothing</a></li>
                                    <li class="link_container"><a href="#">Kid's Clothing</a></li>
                                    <li class="link_container"><a href="#">Women's Clothing</a></li>
                                    <li class="link_container"><a href="#">Accessories</a></li>
                                </ul>
                            </li>
                            <div class="menu-banner-container">
                                <div class="block-container col-sm-6 col-xs-6">
                                    <a href="#">
                                        <img class="img-responsive" src="assets/data/option6/menu_banner1.jpg"
                                             alt="sport">
                                    </a>
                                </div>
                                <div class="block-container col-sm-6 col-xs-6">
                                    <a href="#">
                                        <img class="img-responsive" src="assets/data/option6/menu_banner2.jpg"
                                             alt="sport">
                                    </a>
                                </div>
                            </div>
                        </ul>
                    </li>
                    <li><a href="category.php">Sports</a></li>
                    <li class="dropdown">
                        <a href="category.php" class="dropdown-toggle" data-toggle="dropdown">Foods</a>
                        <ul class="dropdown-menu mega_dropdown" style="width: 830px;">
                            <li class="block-container col-sm-3">
                                <ul class="block">
                                    <li class="link_container group_header">
                                        <a href="#">ASIAN</a>
                                    </li>
                                    <li class="link_container">
                                        <a href="#">Vietnamese Pho</a>
                                    </li>
                                    <li class="link_container">
                                        <a href="#">Noodles</a>
                                    </li>
                                    <li class="link_container">
                                        <a href="#">Seafood</a>
                                    </li>
                                </ul>
                            </li>
                            <li class="block-container col-sm-3">
                                <ul class="block">
                                    <li class="link_container group_header">
                                        <a href="#">Sausages</a>
                                    </li>
                                    <li class="link_container">
                                        <a href="#">Meat Dishes</a>
                                    </li>
                                    <li class="link_container">
                                        <a href="#">Desserts</a>
                                    </li>
                                    <li class="link_container">
                                        <a href="#">Tops</a>
                                    </li>
                                    <li class="link_container">
                                        <a href="#">Tops</a>
                                    </li>
                                </ul>
                            </li>
                            <li class="block-container col-sm-3">
                                <ul class="block">
                                    <li class="link_container group_header">
                                        <a href="#">European</a>
                                    </li>
                                    <li class="link_container">
                                        <a href="#">Greek Potatoes</a>
                                    </li>
                                    <li class="link_container">
                                        <a href="#">Famous Spaghetti</a>
                                    </li>
                                    <li class="link_container">
                                        <a href="#">Famous Spaghetti</a>
                                    </li>
                                </ul>
                            </li>
                            <li class="block-container col-sm-3">
                                <ul class="block">
                                    <li class="link_container group_header">
                                        <a href="#">Chicken</a>
                                    </li>
                                    <li class="link_container">
                                        <a href="#">Italian Pizza</a>
                                    </li>
                                    <li class="link_container">
                                        <a href="#">French Cakes</a>
                                    </li>
                                    <li class="link_container">
                                        <a href="#">Tops</a>
                                    </li>
                                    <li class="link_container">
                                        <a href="#">Tops</a>
                                    </li>
                                </ul>
                            </li>
                            <div class="menu-banner-container">
                                <div class="block-container col-sm-6 col-xs-6">
                                    <a href="#">
                                        <img class="img-responsive" src="assets/data/option6/menu_banner1.jpg"
                                             alt="sport">
                                    </a>
                                </div>
                                <div class="block-container col-sm-6 col-xs-6">
                                    <a href="#">
                                        <img class="img-responsive" src="assets/data/option6/menu_banner2.jpg"
                                             alt="sport">
                                    </a>
                                </div>
                            </div>
                        </ul>
                    </li>
                    <li class="dropdown">
                        <a href="category.php" class="dropdown-toggle" data-toggle="dropdown">Digital</a>

                        <ul class="dropdown-menu mega_dropdown" style="width: 830px;">
                            <li class="block-container col-sm-3">
                                <ul class="block">
                                    <li class="link_container group_header">
                                        <a href="#">MOBILE</a>
                                    </li>
                                    <li class="link_container">
                                        <a href="#">iPhone</a>
                                    </li>
                                    <li class="link_container">
                                        <a href="#">Samsung</a>
                                    </li>
                                    <li class="link_container">
                                        <a href="#">Lenovo</a>
                                    </li>
                                </ul>
                            </li>
                            <li class="block-container col-sm-3">
                                <ul class="block">
                                    <li class="link_container group_header">
                                        <a href="#">TABLET</a>
                                    </li>
                                    <li class="link_container">
                                        <a href="#">Samsung</a>
                                    </li>
                                    <li class="link_container">
                                        <a href="#">Lenovo</a>
                                    </li>
                                    <li class="link_container">
                                        <a href="#">iPhone</a>
                                    </li>
                                </ul>
                            </li>
                            <li class="block-container col-sm-3">
                                <ul class="block">
                                    <li class="link_container group_header">
                                        <a href="#">LAPTOP</a>
                                    </li>
                                    <li class="link_container">
                                        <a href="#">Samsung</a>
                                    </li>
                                    <li class="link_container">
                                        <a href="#">Lenovo</a>
                                    </li>
                                    <li class="link_container">
                                        <a href="#">Apple</a>
                                    </li>
                                </ul>
                            </li>
                            <li class="block-container col-sm-3">
                                <ul class="block">
                                    <li class="link_container group_header">
                                        <a href="#">MEMORY CARDS</a>
                                    </li>
                                    <li class="link_container">
                                        <a href="#">Scandisk</a>
                                    </li>
                                </ul>
                            </li>
                            <div class="menu-banner-container">
                                <div class="block-container col-sm-6 col-xs-6">
                                    <a href="#">
                                        <img class="img-responsive" src="assets/data/option6/menu_banner1.jpg"
                                             alt="sport">
                                    </a>
                                </div>
                                <div class="block-container col-sm-6 col-xs-6">
                                    <a href="#">
                                        <img class="img-responsive" src="assets/data/option6/menu_banner2.jpg"
                                             alt="sport">
                                    </a>
                                </div>
                            </div>
                        </ul>
                    </li>
                    <li><a href="category.php">Furniture</a></li>
                    <li><a href="category.php">Jewelry</a></li>
                </ul>
            </div><!--/.nav-collapse -->
            <div id="mobile-language">
                <ul>
                    <li>
                        <a href="">English</a>
                    </li>
                    <li>
                        <a href="">中文</a>
                    </li>
                </ul>
            </div>
            <div class="col-xs-7 col-sm-8 col-md-7 header-search-box">
                <form class="form-inline">
                    <div class="form-group form-category">
                        <select class="select-category">
                            <option value="2">All Categories</option>
                            <option value="1">Men</option>
                            <option value="2">Women</option>
                        </select>
                    </div>
                    <div class="form-group input-serach">
                        <input type="text" placeholder="Keyword here...">
                    </div>
                    <a class="pull-right btn-search" href="search.php"></a>
                </form>
                <div class="keyword">
                    <p class="lebal">Keywords:</p>
                    <p>
                        <a href="#">T-shirt, </a>
                        <a href="#">Leggings, </a>
                        <a href="#">Cotton, ...</a>
                    </p>
                </div>
            </div>
            <div class="col-xs-5 col-sm-3 col-md-2 group-button-header hide-mobile">
                <div class="btn-cart" id="cart-block">
                    <a title="My cart" href="cart.html">Cart</a>
                    <span class="notify notify-right">2</span>
                    <div class="cart-block">
                        <div class="cart-block-content">
                            <h5 class="cart-title">2 Items in my cart</h5>
                            <div class="cart-block-list">
                                <ul>
                                    <li class="product-info">
                                        <div class="p-left">
                                            <a href="#" class="delete-prod"><i class="fa fa-close"></i></a>
                                            <a href="#">
                                                <img class="img-responsive"
                                                     src="assets/data/option6/product-100x122.jpg" alt="p10">
                                            </a>
                                        </div>
                                        <div class="p-right">
                                            <p class="p-name">Donec Ac Tempus</p>
                                            <div class="content_price">
                                                <p class="price product-price">Retail price</p>
                                                <p>$138,95</p>
                                            </div>
                                            <div class="vcoin-wrapper vcoin-discounted-wrapper">
                                                <span class="product-coin discounted-vcoin">84.15</span>
                                                <span class="product-coin original-vcoin">99</span>
                                            </div>
                                            <div class="p-quantity">Qty: 1</div>
                                        </div>
                                    </li>
                                    <li class="product-info">
                                        <div class="p-left">
                                            <a href="#" class="delete-prod"><i class="fa fa-close"></i></a>
                                            <a href="#">
                                                <img class="img-responsive"
                                                     src="assets/data/option6/product-s5-100x122.jpg" alt="p10">
                                            </a>
                                        </div>
                                        <div class="p-right">
                                            <p class="p-name">Donec Ac Tempus</p>
                                            <div class="content_price">
                                                <p class="price product-price">Retail price</p>
                                                <p>$138,95</p>
                                            </div>
                                            <div class="product-coin">99</div>
                                            <div class="p-quantity">Qty: 1</div>
                                        </div>
                                    </li>
                                </ul>
                            </div>
                            <div class="toal-cart">
                                <span>Total</span>
                                <div class="product-coin pull-right">183.15</div>
                            </div>
                            <div class="cart-buttons">
                                <a href="order.php" class="btn-check-out">Checkout</a>
                            </div>
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </div>
    <!-- END MANIN HEADER -->
    <div id="nav-top-menu" class="nav-top-menu">
        <div class="container">
            <div class="row">
                <div class="col-sm-3" id="box-vertical-megamenus">
                    <div class="box-vertical-megamenus">
                        <h4 class="title">
                            <span class="btn-open-mobile"><i class="fa fa-bars"></i></span>
                        </h4>
                        <div class="vertical-menu-content is-home">
                            <ul class="vertical-menu-list">
                                <li><a href="category.php"><img class="icon-menu" alt="Funky roots"
                                                                src="assets/data/option6/12.png">Electronics</a></li>
                                <li>
                                    <a class="parent" href="category2.html"><img class="icon-menu" alt="Funky roots"
                                                                                 src="assets/data/option6/13.png">Sports
                                        &amp; Outdoors</a>
                                    <div class="vertical-dropdown-menu">
                                        <div class="vertical-groups col-sm-12">
                                            <div class="mega-group col-sm-4">
                                                <h4 class="mega-group-header"><span>Tennis</span></h4>
                                                <ul class="group-link-default">
                                                    <li><a href="#">Tennis</a></li>
                                                    <li><a href="#">Coats &amp; Jackets</a></li>
                                                    <li><a href="#">Blouses &amp; Shirts</a></li>
                                                    <li><a href="#">Tops &amp; Tees</a></li>
                                                    <li><a href="#">Hoodies &amp; Sweatshirts</a></li>
                                                    <li><a href="#">Intimates</a></li>
                                                </ul>
                                            </div>
                                            <div class="mega-group col-sm-4">
                                                <h4 class="mega-group-header"><span>Swimming</span></h4>
                                                <ul class="group-link-default">
                                                    <li><a href="#">Dresses</a></li>
                                                    <li><a href="#">Coats &amp; Jackets</a></li>
                                                    <li><a href="#">Blouses &amp; Shirts</a></li>
                                                    <li><a href="#">Tops &amp; Tees</a></li>
                                                    <li><a href="#">Hoodies &amp; Sweatshirts</a></li>
                                                    <li><a href="#">Intimates</a></li>
                                                </ul>
                                            </div>
                                            <div class="mega-group col-sm-4">
                                                <h4 class="mega-group-header"><span>Shoes</span></h4>
                                                <ul class="group-link-default">
                                                    <li><a href="#">Dresses</a></li>
                                                    <li><a href="#">Coats &amp; Jackets</a></li>
                                                    <li><a href="#">Blouses &amp; Shirts</a></li>
                                                    <li><a href="#">Tops &amp; Tees</a></li>
                                                    <li><a href="#">Hoodies &amp; Sweatshirts</a></li>
                                                    <li><a href="#">Intimates</a></li>
                                                </ul>
                                            </div>
                                            <div class="menu-banner-container">
                                                <div class="block-container col-sm-6 col-xs-6">
                                                    <a href="#">
                                                        <img class="img-responsive"
                                                             src="assets/data/option6/menu_banner1.jpg"
                                                             alt="sport">
                                                    </a>
                                                </div>
                                                <div class="block-container col-sm-6 col-xs-6">
                                                    <a href="#">
                                                        <img class="img-responsive"
                                                             src="assets/data/option6/menu_banner2.jpg"
                                                             alt="sport">
                                                    </a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </li>
                                <li><a href="category.php"><img class="icon-menu" alt="Funky roots"
                                                                src="assets/data/option6/14.png">Smartphone &amp;
                                        Tablets</a></li>
                                <li><a href="category.php"><img class="icon-menu" alt="Funky roots"
                                                                src="assets/data/option6/15.png">Health &amp; Beauty
                                        Bags</a></li>
                                <li>
                                    <a class="parent" href="category.php">
                                        <img class="icon-menu" alt="Funky roots" src="assets/data/option6/16.png">Shoes
                                        &amp; Accessories</a>
                                    <div class="vertical-dropdown-menu">
                                        <div class="vertical-groups col-sm-12">
                                            <div class="mega-group col-sm-12">
                                                <h4 class="mega-group-header"><span>Special products</span></h4>
                                                <div class="row mega-products">
                                                    <div class="col-sm-3 mega-product">
                                                        <div class="product-avatar">
                                                            <a href="#"><img src="assets/data/option6/p10.jpg"
                                                                             alt="product1"></a>
                                                        </div>
                                                        <div class="product-name">
                                                            <a href="#">Fashion hand bag</a>
                                                        </div>
                                                        <div class="product-price">
                                                            <div class="new-price">$38</div>
                                                            <div class="old-price">$45</div>
                                                        </div>
                                                        <div class="product-star">
                                                            <i class="fa fa-star"></i>

                                                            <i class="fa fa-star"></i>
                                                            <i class="fa fa-star"></i>
                                                            <i class="fa fa-star"></i>
                                                            <i class="fa fa-star-half-o"></i>
                                                        </div>
                                                    </div>
                                                    <div class="col-sm-3 mega-product">
                                                        <div class="product-avatar">
                                                            <a href="#"><img src="assets/data/option6/p11.jpg"
                                                                             alt="product1"></a>
                                                        </div>
                                                        <div class="product-name">
                                                            <a href="#">Fashion hand bag</a>
                                                        </div>
                                                        <div class="product-price">
                                                            <div class="new-price">$38</div>
                                                            <div class="old-price">$45</div>
                                                        </div>
                                                        <div class="product-star">
                                                            <i class="fa fa-star"></i>
                                                            <i class="fa fa-star"></i>
                                                            <i class="fa fa-star"></i>
                                                            <i class="fa fa-star"></i>
                                                            <i class="fa fa-star-half-o"></i>
                                                        </div>
                                                    </div>
                                                    <div class="col-sm-3 mega-product">
                                                        <div class="product-avatar">
                                                            <a href="#"><img src="assets/data/option6/p12.jpg"
                                                                             alt="product1"></a>
                                                        </div>
                                                        <div class="product-name">
                                                            <a href="#">Fashion hand bag</a>
                                                        </div>
                                                        <div class="product-price">
                                                            <div class="new-price">$38</div>
                                                            <div class="old-price">$45</div>
                                                        </div>
                                                        <div class="product-star">
                                                            <i class="fa fa-star"></i>
                                                            <i class="fa fa-star"></i>
                                                            <i class="fa fa-star"></i>
                                                            <i class="fa fa-star"></i>
                                                            <i class="fa fa-star-half-o"></i>
                                                        </div>
                                                    </div>
                                                    <div class="col-sm-3 mega-product">
                                                        <div class="product-avatar">
                                                            <a href="#"><img src="assets/data/option6/p13.jpg"
                                                                             alt="product1"></a>
                                                        </div>
                                                        <div class="product-name">
                                                            <a href="#">Fashion hand bag</a>
                                                        </div>
                                                        <div class="product-price">
                                                            <div class="new-price">$38</div>
                                                            <div class="old-price">$45</div>
                                                        </div>
                                                        <div class="product-star">
                                                            <i class="fa fa-star"></i>
                                                            <i class="fa fa-star"></i>
                                                            <i class="fa fa-star"></i>
                                                            <i class="fa fa-star"></i>
                                                            <i class="fa fa-star-half-o"></i>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </li>
                                <li><a href="category.php"><img class="icon-menu" alt="Funky roots"
                                                                src="assets/data/option6/17.png">Toys &amp; Hobbies</a>
                                </li>
                                <li><a href="category.php"><img class="icon-menu" alt="Funky roots"
                                                                src="assets/data/option6/18.png">Computers &amp;
                                        Networking</a></li>
                                <li><a href="category.php"><img class="icon-menu" alt="Funky roots"
                                                                src="assets/data/option6/19.png">Laptops &amp;
                                        Accessories</a></li>
                                <li><a href="category.php"><img class="icon-menu" alt="Funky roots"
                                                                src="assets/data/option6/20.png">Jewelry &amp;
                                        Watches</a>
                                </li>
                                <li><a href="category.php"><img class="icon-menu" alt="Funky roots"
                                                                src="assets/data/option6/21.png">Flashlights &amp;
                                        Lamps</a></li>
                                <li>
                                    <a href="category.php">
                                        <img class="icon-menu" alt="Funky roots" src="assets/data/option6/21.png">
                                        Cameras &amp; Photo
                                    </a>
                                </li>
                                <li class="cat-link-orther">
                                    <a href="category.php">
                                        <img class="icon-menu" alt="Funky roots" src="assets/data/option6/22.png">
                                        Television
                                    </a>
                                </li>
                                <li class="cat-link-orther">
                                    <a href="category.php">
                                        <img class="icon-menu" alt="Funky roots" src="assets/data/option6/12.png">Computers
                                        &amp; Networking
                                    </a>
                                </li>
                                <li class="cat-link-orther">
                                    <a href="category.php">
                                        <img class="icon-menu" alt="Funky roots" src="assets/data/option6/14.png">
                                        Toys &amp; Hobbies
                                    </a>
                                </li>
                                <li class="cat-link-orther">
                                    <a href="category.php"><img class="icon-menu" alt="Funky roots"
                                                                src="assets/data/option6/17.png">Jewelry &amp;
                                        Watches</a>
                                </li>
                            </ul>
                            <div class="all-category"><span class="open-cate">All Categories</span></div>
                        </div>
                    </div>
                </div>
                <div id="main-menu" class="col-sm-9 main-menu hide-mobile">
                    <nav class="navbar navbar-default">
                        <div class="container-fluid">
                            <div class="navbar-header">
                                <button type="button" class="navbar-toggle collapsed" data-toggle="collapse"
                                        data-target="#navbar" aria-expanded="false" aria-controls="navbar">
                                    <i class="fa fa-bars"></i>
                                </button>
                                <a class="navbar-brand" href="#">MENU</a>
                            </div>
                            <div id="navbar" class="navbar-collapse collapse">
                                <ul class="nav navbar-nav">
                                    <li class="active">
                                        <a href="#">Home</a>
                                    </li>
                                    <li class="dropdown">
                                        <a href="category.php" class="dropdown-toggle"
                                           data-toggle="dropdown">Fashion</a>
                                        <ul class="dropdown-menu mega_dropdown" role="menu" style="width: 830px;">
                                            <li class="block-container col-sm-3">
                                                <ul class="block">
                                                    <li class="link_container group_header">
                                                        <a href="#">MEN'S</a>
                                                    </li>
                                                    <li class="link_container"><a href="#">Skirts</a></li>
                                                    <li class="link_container"><a href="#">Jackets</a></li>
                                                    <li class="link_container"><a href="#">Tops</a></li>
                                                    <li class="link_container"><a href="#">Scarves</a></li>
                                                    <li class="link_container"><a href="#">Pants</a></li>
                                                </ul>
                                            </li>
                                            <li class="block-container col-sm-3">
                                                <ul class="block">
                                                    <li class="link_container group_header">
                                                        <a href="#">WOMEN'S</a>
                                                    </li>
                                                    <li class="link_container"><a href="#">Skirts</a></li>
                                                    <li class="link_container"><a href="#">Jackets</a></li>
                                                    <li class="link_container"><a href="#">Tops</a></li>
                                                    <li class="link_container"><a href="#">Scarves</a></li>
                                                    <li class="link_container"><a href="#">Pants</a></li>
                                                </ul>
                                            </li>
                                            <li class="block-container col-sm-3">
                                                <ul class="block">
                                                    <li class="link_container group_header">
                                                        <a href="#">Kids</a>
                                                    </li>
                                                    <li class="link_container"><a href="#">Shoes</a></li>
                                                    <li class="link_container"><a href="#">Clothing</a></li>
                                                    <li class="link_container"><a href="#">Tops</a></li>
                                                    <li class="link_container"><a href="#">Scarves</a></li>
                                                    <li class="link_container"><a href="#">Accessories</a></li>
                                                </ul>
                                            </li>
                                            <li class="block-container col-sm-3">
                                                <ul class="block">
                                                    <li class="link_container group_header">
                                                        <a href="#">TRENDING</a>
                                                    </li>
                                                    <li class="link_container"><a href="#">Men's Clothing</a></li>
                                                    <li class="link_container"><a href="#">Kid's Clothing</a></li>
                                                    <li class="link_container"><a href="#">Women's Clothing</a></li>
                                                    <li class="link_container"><a href="#">Accessories</a></li>
                                                </ul>
                                            </li>
                                            <div class="block-container menu-banner-container col-sm-6">
                                                <a href="#">
                                                    <img class="img-responsive"
                                                         src="assets/data/option6/menu_banner1.jpg" alt="sport">
                                                </a>
                                            </div>
                                            <div class="block-container menu-banner-container col-sm-6">
                                                <a href="#">
                                                    <img class="img-responsive"
                                                         src="assets/data/option6/menu_banner2.jpg" alt="sport">
                                                </a>
                                            </div>
                                        </ul>
                                    </li>
                                    <li><a href="category.php">Sports</a></li>
                                    <li class="dropdown">
                                        <a href="category.php" class="dropdown-toggle" data-toggle="dropdown">Foods</a>
                                        <ul class="dropdown-menu mega_dropdown" style="width: 830px;">
                                            <li class="block-container col-sm-3">
                                                <ul class="block">
                                                    <li class="link_container group_header">
                                                        <a href="#">ASIAN</a>
                                                    </li>
                                                    <li class="link_container">
                                                        <a href="#">Vietnamese Pho</a>
                                                    </li>
                                                    <li class="link_container">
                                                        <a href="#">Noodles</a>
                                                    </li>
                                                    <li class="link_container">
                                                        <a href="#">Seafood</a>
                                                    </li>
                                                </ul>
                                            </li>
                                            <li class="block-container col-sm-3">
                                                <ul class="block">
                                                    <li class="link_container group_header">
                                                        <a href="#">Sausages</a>
                                                    </li>
                                                    <li class="link_container">
                                                        <a href="#">Meat Dishes</a>
                                                    </li>
                                                    <li class="link_container">
                                                        <a href="#">Desserts</a>
                                                    </li>
                                                    <li class="link_container">
                                                        <a href="#">Tops</a>
                                                    </li>
                                                    <li class="link_container">
                                                        <a href="#">Tops</a>
                                                    </li>
                                                </ul>
                                            </li>
                                            <li class="block-container col-sm-3">
                                                <ul class="block">
                                                    <li class="link_container group_header">
                                                        <a href="#">European</a>
                                                    </li>
                                                    <li class="link_container">
                                                        <a href="#">Greek Potatoes</a>
                                                    </li>
                                                    <li class="link_container">
                                                        <a href="#">Famous Spaghetti</a>
                                                    </li>
                                                    <li class="link_container">
                                                        <a href="#">Famous Spaghetti</a>
                                                    </li>
                                                </ul>
                                            </li>
                                            <li class="block-container col-sm-3">
                                                <ul class="block">
                                                    <li class="link_container group_header">
                                                        <a href="#">Chicken</a>
                                                    </li>
                                                    <li class="link_container">
                                                        <a href="#">Italian Pizza</a>
                                                    </li>
                                                    <li class="link_container">
                                                        <a href="#">French Cakes</a>
                                                    </li>
                                                    <li class="link_container">
                                                        <a href="#">Tops</a>
                                                    </li>
                                                    <li class="link_container">
                                                        <a href="#">Tops</a>
                                                    </li>
                                                </ul>
                                            </li>
                                            <div class="block-container menu-banner-container col-sm-6">
                                                <a href="#">
                                                    <img class="img-responsive"
                                                         src="assets/data/option6/menu_banner1.jpg" alt="sport">
                                                </a>
                                            </div>
                                            <div class="block-container menu-banner-container col-sm-6">
                                                <a href="#">
                                                    <img class="img-responsive"
                                                         src="assets/data/option6/menu_banner2.jpg" alt="sport">
                                                </a>
                                            </div>
                                        </ul>
                                    </li>
                                    <li class="dropdown">
                                        <a href="category.php" class="dropdown-toggle"
                                           data-toggle="dropdown">Digital</a>

                                        <ul class="dropdown-menu mega_dropdown" style="width: 830px;">
                                            <li class="block-container col-sm-3">
                                                <ul class="block">
                                                    <li class="link_container group_header">
                                                        <a href="#">MOBILE</a>
                                                    </li>
                                                    <li class="link_container">
                                                        <a href="#">iPhone</a>
                                                    </li>
                                                    <li class="link_container">
                                                        <a href="#">Samsung</a>
                                                    </li>
                                                    <li class="link_container">
                                                        <a href="#">Lenovo</a>
                                                    </li>
                                                </ul>
                                            </li>
                                            <li class="block-container col-sm-3">
                                                <ul class="block">
                                                    <li class="link_container group_header">
                                                        <a href="#">TABLET</a>
                                                    </li>
                                                    <li class="link_container">
                                                        <a href="#">Samsung</a>
                                                    </li>
                                                    <li class="link_container">
                                                        <a href="#">Lenovo</a>
                                                    </li>
                                                    <li class="link_container">
                                                        <a href="#">iPhone</a>
                                                    </li>
                                                </ul>
                                            </li>
                                            <li class="block-container col-sm-3">
                                                <ul class="block">
                                                    <li class="link_container group_header">
                                                        <a href="#">LAPTOP</a>
                                                    </li>
                                                    <li class="link_container">
                                                        <a href="#">Samsung</a>
                                                    </li>
                                                    <li class="link_container">
                                                        <a href="#">Lenovo</a>
                                                    </li>
                                                    <li class="link_container">
                                                        <a href="#">Apple</a>
                                                    </li>
                                                </ul>
                                            </li>
                                            <li class="block-container col-sm-3">
                                                <ul class="block">
                                                    <li class="link_container group_header">
                                                        <a href="#">MEMORY CARDS</a>
                                                    </li>
                                                    <li class="link_container">
                                                        <a href="#">Scandisk</a>
                                                    </li>
                                                </ul>
                                            </li>
                                            <div class="block-container menu-banner-container col-sm-6">
                                                <a href="#">
                                                    <img class="img-responsive"
                                                         src="assets/data/option6/menu_banner1.jpg" alt="sport">
                                                </a>
                                            </div>
                                            <div class="block-container menu-banner-container col-sm-6">
                                                <a href="#">
                                                    <img class="img-responsive"
                                                         src="assets/data/option6/menu_banner2.jpg" alt="sport">
                                                </a>
                                            </div>
                                        </ul>
                                    </li>
                                    <li><a href="category.php">Furniture</a></li>
                                    <li><a href="category.php">Jewelry</a></li>
                                </ul>
                            </div><!--/.nav-collapse -->
                        </div>
                    </nav>
                </div>
            </div>
            <!-- userinfo on top-->
            <div id="form-search-opntop">
            </div>
            <!-- userinfo on top-->
            <div id="user-info-opntop">
            </div>
            <!-- CART ICON ON MMENU -->
            <div id="shopping-cart-box-ontop">
                <i class="fa fa-shopping-cart"></i>
                <div class="shopping-cart-box-ontop-content"></div>
            </div>
        </div>
    </div>
</div>
<!-- end header -->