<?php include("includes/header.php"); ?>
<body>
<?php include("includes/navigation.php"); ?>
<!-- page wapper-->
<div class="columns-container">
    <div class="container" id="columns">
        <!-- breadcrumb -->
        <div class="breadcrumb clearfix">
            <a class="home" href="#" title="Return to Home">Home</a>
            <span class="navigation-pipe">&nbsp;</span>
            <span class="navigation_page">My Bid History</span>
        </div>
        <!-- ./breadcrumb -->
        <!-- row -->
        <div class="row">
            <!-- Left colunm -->
            <div class="column col-xs-12 col-sm-3" id="left_column">
                <!-- block category -->
                <div class="block left-module">
                    <p class="title_block btn-filter">Account Information</p>
                    <div class="block_content">
                        <!-- layered -->
                        <div class="layered layered-category">
                            <div class="layered-content">
                                <ul class="tree-menu">
                                    <li><span></span><a href="my_account.php">My Account</a></li>
                                    <li><span></span><a href="my_password.php">My Password</a></li>
                                    <li><span></span><a href="my_order.php">My Order</a></li>
                                    <li class="active"><span></span><a>My Bid History</a></li>
                                    <li><span></span><a href="my_shipping_address.php">My Shipping Address</a></li>
                                </ul>
                            </div>
                        </div>
                        <!-- ./layered -->
                    </div>
                </div>
                <!-- ./block category  -->
            </div>
            <!-- ./left colunm -->
            <!-- Center colunm-->
            <div class="center_column col-xs-12 col-sm-9 my-account-wrapper" id="center_column">
                <!-- page heading-->
                <h2 class="page-heading">
                    <span class="page-heading-title2">My Bid History</span>
                </h2>
                <!-- Content page -->
                <div class="content-text clearfix">
                    <div class="box-border">
                        <div class="table-responsive">
                            <table class="table table-bordered cart_summary" id="tbl-bid-history">
                                <thead>
                                <tr>
                                    <th>ID</th>
                                    <th>Product Name</th>
                                    <th>Retail Price</th>
                                    <th class="text-center">GPoint / Bid</th>
                                    <th class="text-center">Bid Date</th>
                                    <th class="text-center">Status</th>
                                </tr>
                                </thead>
                                <tbody>
                                <tr>
                                    <td>
                                        <a href="#">12423245</a>
                                    </td>
                                    <td><span>Product name</span></td>
                                    <td>
                                        <a href="">RM 99.00</a>
                                    </td>
                                    <td class="text-center">1234</td>
                                    <td class="text-center">21/7/2016</td>
                                    <td class="text-center">Won</td>
                                </tr>
                                <tr>
                                    <td>
                                        <a href="#">9768712345</a>
                                    </td>
                                    <td><span>Product name</span></td>
                                    <td>
                                        <a href="">RM 99.00</a>
                                    </td>
                                    <td class="text-center">1234</td>
                                    <td class="text-center">21/7/2016</td>
                                    <td class="text-center">Lost</td>
                                </tr>
                                <tr>
                                    <td>
                                        <a href="#">2342</a>
                                    </td>
                                    <td><span>Product name</span></td>
                                    <td>
                                        <a href="">RM 99.00</a>
                                    </td>
                                    <td class="text-center">1234</td>
                                    <td class="text-center">21/7/2016</td>
                                    <td class="text-center">Won</td>
                                </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
                <!-- ./Content page -->
            </div>
            <!-- ./ Center colunm -->
        </div>
        <!-- ./row-->
    </div>
</div>
<!-- ./page wapper-->
<?php include("includes/footer.php"); ?>
<?php include("includes/scripts.php"); ?>
</body>
</html>