<?php include("includes/header.php"); ?>
<body>
<?php include("includes/navigation.php"); ?>
<!-- page wapper-->
<div class="columns-container">
    <div class="container" id="columns">
        <!-- breadcrumb -->
        <div class="breadcrumb clearfix">
            <a class="home" href="#" title="Return to Home">Home</a>
            <span class="navigation-pipe">&nbsp;</span>
            <span class="navigation_page">My shipping address</span>
        </div>
        <!-- ./breadcrumb -->
        <!-- row -->
        <div class="row">
            <!-- Left colunm -->
            <div class="column col-xs-12 col-sm-3" id="left_column">
                <!-- block category -->
                <div class="block left-module">
                    <p class="title_block btn-filter">Account Information</p>
                    <div class="block_content">
                        <!-- layered -->
                        <div class="layered layered-category">
                            <div class="layered-content">
                                <ul class="tree-menu">
                                    <li><span></span><a href="my_account.php">My Account</a></li>
                                    <li><span></span><a href="my_password.php">My Password</a></li>
                                    <li><span></span><a href="my_order.php">My Order</a></li>
                                    <li><span></span><a href="my_bid_history.php">My Bid History</a></li>
                                    <li class="active"><span></span><a>My Shipping Address</a></li>
                                </ul>
                            </div>
                        </div>
                        <!-- ./layered -->
                    </div>
                </div>
                <!-- ./block category  -->
            </div>
            <!-- ./left colunm -->
            <!-- Center colunm-->
            <div class="center_column col-xs-12 col-sm-9 my-account-wrapper" id="center_column">
                <!-- page heading-->
                <h2 class="page-heading">
                    <span class="page-heading-title2">My Shipping Address</span>
                </h2>
                <!-- Content page -->
                <div class="content-text clearfix">
                    <div class="box-border">
                        <form class="form-horizontal clearfix">
                            <div class="form-group">
                                <label class="col-md-3 control-label">Address Line 1</label>
                                <div class="col-md-9">
                                    <input class="form-control input" placeholder="Please enter address line 1">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-3 control-label">Address Line 2</label>
                                <div class="col-md-9">
                                    <input class="form-control input" placeholder="Please enter address line 2">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-3 control-label">City</label>
                                <div class="col-md-9">
                                    <input class="form-control input" placeholder="Please enter city">
                                </div>
                            </div>
                            <div class="form-group select-bar">
                                <label class="col-md-3 control-label">Country</label>
                                <div class="col-md-9">
                                    <select class="form-control input">
                                        <option>-- Please select country --</option>
                                        <option>1</option>
                                        <option>1</option>
                                    </select>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-3 control-label">Zip Code</label>
                                <div class="col-md-9">
                                    <input class="form-control input" placeholder="Please enter zip code">
                                </div>
                            </div>
                            <div class="form-group text-center">
                                <button type="submit" class="button">Submit</button>
                            </div>
                        </form>
                    </div>
                </div>
                <!-- ./Content page -->
            </div>
            <!-- ./ Center colunm -->
        </div>
        <!-- ./row-->
    </div>
</div>
<!-- ./page wapper-->
<?php include("includes/footer.php"); ?>
<?php include("includes/scripts.php"); ?>
</body>
</html>