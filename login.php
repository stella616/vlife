<?php include("includes/header.php"); ?>
<body>
<?php include("includes/navigation.php"); ?>
<!-- page wapper-->
<div class="columns-container">
    <div class="container" id="columns">
        <!-- breadcrumb -->
        <div class="breadcrumb clearfix">
            <a class="home" href="#" title="Return to Home">Home</a>
            <span class="navigation-pipe">&nbsp;</span>
            <span class="navigation_page">Authentication</span>
        </div>
        <!-- ./breadcrumb -->
        <!-- page heading-->
        <h2 class="page-heading">
            <span class="page-heading-title2">Authentication</span>
        </h2>
        <!-- ../page heading-->
        <div class="page-content">
            <div class="row">
                <form class="form-styling">
                    <div class="col-sm-6">
                        <div class="box-authentication">
                            <h3>Create an account</h3>
                            <p>Please enter your email address to create an account.</p>
                            <div class="form-group">
                                <label for="email_register" class="control-label">Email address</label>
                                <input id="email_register" type="text" class="form-control">
                            </div>
                            <button class="button"><i class="fa fa-user"></i>Create an account</button>
                        </div>
                    </div>
                    <div class="col-sm-6">
                        <div class="box-authentication box-login">
                            <h3>Already registered?</h3>
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="email_login" class="control-label">Email address</label>
                                        <input id="email_login" type="text" class="form-control">
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="password_login" class="control-label">Password</label>
                                        <input id="password_login" type="password" class="form-control">
                                    </div>
                                </div>
                            </div>
                            <p class="forgot-pass"><a href="forgot_password.php">Forgot your password?</a></p>
                            <button class="button"><i class="fa fa-lock"></i>Sign in</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<!-- ./page wapper-->
<?php include("includes/footer.php"); ?>
<?php include("includes/scripts.php"); ?>
</body>
</html>