<?php include("includes/header.php"); ?>
<body>
<?php include("includes/navigation.php"); ?>
<!-- page wapper-->
<div class="columns-container">
    <div class="container" id="columns">
        <!-- breadcrumb -->
        <div class="breadcrumb clearfix">
            <a class="home" href="#" title="Return to Home">Home</a>
            <span class="navigation-pipe">&nbsp;</span>
            <span class="navigation_page">Checkout</span>
        </div>
        <!-- ./breadcrumb -->
        <!-- page heading-->
        <h2 class="page-heading">
            <span class="page-heading-title2">Checkout</span>
        </h2>
        <!-- ../page heading-->
        <div class="page-content checkout-page">
            <h3 class="checkout-sep">1. Checkout Method</h3>
            <div class="checkout-step1">
                <div class="checkout-step1-inner">
                    <div class="box-border">
                        <div class="checkout-register-wrapper">
                            <h3>Checkout as a Guest or Register</h3>
                            <p>Register with us for future convenience:</p>
                            <ul>
                                <li><label><input type="radio" name="radio1">Checkout as Guest</label></li>
                                <li><label><input type="radio" name="radio1">Register</label></li>
                            </ul>
                            <br>
                            <br>
                            <h3>Register and save time!</h3>
                            <p>Register with us for future convenience:</p>
                            <p><i class="fa fa-check-circle text-primary"></i> Fast and easy check out</p>
                            <p><i class="fa fa-check-circle text-primary"></i> Easy access to your order history and
                                status
                            </p>
                            <div class="text-center">
                                <button class="button">Continue</button>
                            </div>
                        </div>
                    </div>
                    <div class="box-border">
                        <div>
                            <h3>Login</h3>
                            <p class="margin-bottom-40">Already registered? Please log in below:</p>
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="email_login" class="control-label">Email address</label>
                                        <input id="email_login" type="text" class="form-control input">
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="password_login" class="control-label">Password</label>
                                        <input id="password_login" type="password" class="form-control input">
                                    </div>
                                </div>
                            </div>
                            <p class="text-center forgot-pass"><a href="forgot_password.php">Forgot your password?</a>
                            </p>
                            <div class="text-center">
                                <button class="button"><i class="fa fa-lock"></i>Sign in</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <h3 class="checkout-sep">2. Billing Information</h3>
            <div class="box-border">
                <ul>
                    <li class="row">
                        <div class="col-sm-6">
                            <div class="form-group">
                                <label for="first_name" class="required">First Name</label>
                                <input type="text" class="input form-control" name="" id="first_name">
                            </div>
                        </div><!--/ [col] -->
                        <div class="col-sm-6">
                            <div class="form-group">
                                <label for="last_name" class="required">Last Name</label>
                                <input type="text" name="" class="input form-control" id="last_name">
                            </div>
                        </div><!--/ [col] -->
                    </li><!--/ .row -->
                    <li class="row">
                        <div class="col-sm-6">
                            <div class="form-group">
                                <label for="company_name">Company Name</label>
                                <input type="text" name="" class="input form-control" id="company_name">
                            </div>
                        </div><!--/ [col] -->
                        <div class="col-sm-6">
                            <div class="form-group">
                                <label for="email_address" class="required">Email Address</label>
                                <input type="text" class="input form-control" name="" id="email_address">
                            </div>
                        </div><!--/ [col] -->
                    </li><!--/ .row -->
                    <li class="row">
                        <div class="col-xs-12">
                            <div class="form-group">
                                <label for="address" class="required">Address</label>
                                <input type="text" class="input form-control" name="" id="address">
                            </div>
                        </div><!--/ [col] -->
                    </li><!-- / .row -->
                    <li class="row">
                        <div class="col-sm-6">
                            <div class="form-group">
                                <label for="city" class="required">City</label>
                                <input class="input form-control" type="text" name="" id="city">
                            </div>
                        </div><!--/ [col] -->
                        <div class="col-sm-6">
                            <div class="form-group">
                                <label class="required">State/Province</label>
                                <select class="input form-control" name="">
                                    <option value="Alabama">Alabama</option>
                                    <option value="Illinois">Illinois</option>
                                    <option value="Kansas">Kansas</option>
                                </select>
                            </div>
                        </div><!--/ [col] -->
                    </li><!--/ .row -->
                    <li class="row">
                        <div class="col-sm-6">
                            <div class="form-group">
                                <label for="postal_code" class="required">Zip/Postal Code</label>
                                <input class="input form-control" type="text" name="" id="postal_code">
                            </div>
                        </div><!--/ [col] -->
                        <div class="col-sm-6">
                            <div class="form-group">
                                <label class="required">Country</label>
                                <select class="input form-control" name="">
                                    <option value="USA">USA</option>
                                    <option value="Australia">Australia</option>
                                    <option value="Austria">Austria</option>
                                    <option value="Argentina">Argentina</option>
                                    <option value="Canada">Canada</option>
                                </select>
                            </div>
                        </div><!--/ [col] -->
                    </li><!--/ .row -->
                    <li class="row">
                        <div class="col-sm-6">
                            <div class="form-group">
                                <label for="telephone" class="required">Telephone</label>
                                <input class="input form-control" type="text" name="" id="telephone">
                            </div>
                        </div><!--/ [col] -->
                        <div class="col-sm-6">
                            <div class="form-group">
                                <label for="fax">Fax</label>
                                <input class="input form-control" type="text" name="" id="fax">
                            </div>
                        </div><!--/ [col] -->
                    </li><!--/ .row -->
                    <li class="text-center">
                        <button class="button">Continue</button>
                    </li>
                </ul>
            </div>
            <h3 class="checkout-sep">3. Shipping Information</h3>
            <div class="box-border">
                <ul>
                    <li class="row">
                        <div class="col-sm-6">
                            <div class="form-group">
                                <label for="first_name_1" class="required">First Name</label>
                                <input class="input form-control" type="text" name="" id="first_name_1">
                            </div>
                        </div><!--/ [col] -->
                        <div class="col-sm-6">
                            <div class="form-group">
                                <label for="last_name_1" class="required">Last Name</label>
                                <input class="input form-control" type="text" name="" id="last_name_1">
                            </div>
                        </div><!--/ [col] -->
                    </li><!--/ .row -->
                    <li class="row">
                        <div class="col-sm-6">
                            <div class="form-group">
                                <label for="company_name_1">Company Name</label>
                                <input class="input form-control" type="text" name="" id="company_name_1">
                            </div>
                        </div><!--/ [col] -->
                        <div class="col-sm-6">
                            <div class="form-group">
                                <label for="email_address_1" class="required">Email Address</label>
                                <input class="input form-control" type="text" name="" id="email_address_1">
                            </div>
                        </div><!--/ [col] -->
                    </li><!--/ .row -->
                    <li class="row">
                        <div class="col-xs-12">
                            <div class="form-group">
                                <label for="address_1" class="required">Address</label>
                                <input class="input form-control" type="text" name="" id="address_1">
                            </div>
                        </div><!--/ [col] -->
                    </li><!--/ .row -->
                    <li class="row">
                        <div class="col-sm-6">
                            <div class="form-group">
                                <label for="city_1" class="required">City</label>
                                <input class="input form-control" type="text" name="" id="city_1">
                            </div>
                        </div><!--/ [col] -->
                        <div class="col-sm-6">
                            <div class="form-group">
                                <label class="required">State/Province</label>
                                <div class="custom_select">
                                    <select class="input form-control" name="">
                                        <option value="Alabama">Alabama</option>
                                        <option value="Illinois">Illinois</option>
                                        <option value="Kansas">Kansas</option>
                                    </select>
                                </div>
                            </div>
                        </div><!--/ [col] -->
                    </li><!--/ .row -->
                    <li class="row">
                        <div class="col-sm-6">
                            <div class="form-group">
                                <label for="postal_code_1" class="required">Zip/Postal Code</label>
                                <input class="input form-control" type="text" name="" id="postal_code_1">
                            </div>
                        </div><!--/ [col] -->
                        <div class="col-sm-6">
                            <div class="form-group">
                                <label class="required">Country</label>
                                <div class="custom_select">
                                    <select class="input form-control" name="">
                                        <option value="USA">USA</option>
                                        <option value="Australia">Australia</option>
                                        <option value="Austria">Austria</option>
                                        <option value="Argentina">Argentina</option>
                                        <option value="Canada">Canada</option>
                                    </select>
                                </div>
                            </div>
                        </div><!--/ [col] -->
                    </li><!--/ .row -->
                    <li class="row">
                        <div class="col-sm-6">
                            <div class="form-group">
                                <label for="telephone_1" class="required">Telephone</label>
                                <input class="input form-control" type="text" name="" id="telephone_1">
                            </div>
                        </div><!--/ [col] -->
                        <div class="col-sm-6">
                            <div class="form-group">
                                <label for="fax_1">Fax</label>
                                <input class="input form-control" type="text" name="" id="fax_1">
                            </div>
                        </div><!--/ [col] -->
                    </li><!--/ .row -->
                    <li class="text-center">
                        <button class="button">Continue</button>
                    </li>
                </ul>
            </div>
            <h3 class="checkout-sep">4. Shipping Method</h3>
            <div class="box-border">
                <div class="row shipping_method margin-bottom-40 clearfix">
                    <div class="col-md-6">
                        <p class="subcaption bold">Free Shipping</p>
                        <label for="radio_button_3"><input type="radio" checked name="radio_3" id="radio_button_3">Free
                            $0</label>
                    </div>
                    <div class="col-md-6">
                        <p class="subcaption bold">Free Shipping</p>
                        <label for="radio_button_4"><input type="radio" name="radio_3" id="radio_button_4"> Standard
                            Shipping $5.00</label>
                    </div>
                </div>
                <div class="text-center">
                    <button class="button">Continue</button>
                </div>
            </div>
            <h3 class="checkout-sep">5. Payment Information</h3>
            <div class="box-border">
                <div class="row margin-bottom-40 clearfix">
                    <div class="col-md-6">
                        <label for="radio_button_5"><input type="radio" checked name="radio_4" id="radio_button_5">
                            Check / Money order</label>
                    </div>
                    <div class="col-md-6">
                        <label for="radio_button_6"><input type="radio" name="radio_4" id="radio_button_6"> Credit card
                            (saved)</label>
                    </div>
                </div>
                <div class="text-center">
                    <button class="button">Continue</button>
                </div>
            </div>
            <h3 class="checkout-sep">6. Order Review</h3>
            <div class="box-border">
                <div class="table-responsive">
                    <table class="table table-bordered cart_summary">
                        <thead>
                        <tr>
                            <th class="action"><i class="fa fa-trash-o"></i></th>
                            <th class="cart_product text-center">Product</th>
                            <th class="cart_product">Product Name</th>
                            <th class="text-center">Total VCoin</th>
                            <th class="text-center">Quantity</th>
                            <th class="text-center">Subtotal</th>
                        </tr>
                        </thead>
                        <tbody>
                        <tr>
                            <td class="action">
                                <a href="#"><i class="fa fa-close"></i></a>
                            </td>
                            <td class="cart_product">
                                <a href="#">
                                    <div class="discounted-percentage product-list-discount-percentage">-15%</div>
                                    <img src="assets/data/product-100x122.jpg" alt="Product">
                                </a>
                            </td>
                            <td class="cart_description">
                                <p class="product-name"><a href="#">Frederique Constant </a></p>
                                <small class="cart_ref">SKU : #123654999</small>
                                <br>
                                <small><a href="#">Color : Beige</a></small>
                                <br>
                                <small><a href="#">Size : S</a></small>
                            </td>
                            <td class="price">
                                <div class="vcoin-wrapper vcoin-discounted-wrapper">
                                    <span class="product-coin discounted-vcoin">84.15</span>
                                    <span class="product-coin original-vcoin">99</span>
                                </div>
                            </td>
                            <td class="qty">
                                <input class="form-control input-sm" type="text" value="1">
                                <a href="#"><i class="fa fa-caret-up"></i></a>
                                <a href="#"><i class="fa fa-caret-down"></i></a>
                            </td>
                            <td class="price">
                                <div class="vcoin-wrapper vcoin-discounted-wrapper">
                                    <span class="product-coin discounted-vcoin">84.15</span>
                                    <span class="product-coin original-vcoin">99</span>
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <td class="action">
                                <a href="#"><i class="fa fa-close"></i></a>
                            </td>
                            <td class="cart_product">
                                <a href="#"><img src="assets/data/product-100x122.jpg" alt="Product"></a>
                            </td>
                            <td class="cart_description">
                                <p class="product-name"><a href="#">Frederique Constant </a></p>
                                <small class="cart_ref">SKU : #123654999</small>
                                <br>
                                <small><a href="#">Color : Beige</a></small>
                                <br>
                                <small><a href="#">Size : S</a></small>
                            </td>
                            <td class="price">
                                <div class="vcoin-wrapper">
                                    <span class="product-coin original-vcoin">99</span>
                                </div>
                            </td>
                            <td class="qty">
                                <input class="form-control input-sm" type="text" value="1">
                                <a href="#"><i class="fa fa-caret-up"></i></a>
                                <a href="#"><i class="fa fa-caret-down"></i></a>
                            </td>
                            <td class="price">
                                <div class="vcoin-wrapper">
                                    <span class="product-coin original-vcoin">99</span>
                                </div>
                            </td>
                        </tr>
                        </tbody>
                        <tfoot>
                        <tr>
                            <td colspan="2" rowspan="2"></td>
                            <td colspan="3">Total products (tax incl.)</td>
                            <td colspan="2"><span class="product-coin original-vcoin">183.15</span></td>
                        </tr>
                        <tr>
                            <td colspan="3"><strong>Total</strong></td>
                            <td colspan="2"><strong><span class="product-coin original-vcoin">183.15</span></strong></td>
                        </tr>
                        </tfoot>
                    </table>
                </div>
                <div class="btn-place-order">
                    <button class="button pull-right">Place Order</button>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- ./page wapper-->
<?php include("includes/footer.php"); ?>
<?php include("includes/scripts.php"); ?>
</body>
</html>